package com.formetco.formetcoxd;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

/**
 * Created by austinsweat on 7/10/15.
 */
public class AccountSettings extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);
        ModelManager.activitySetup(this, false);

        TextView emailTextView = (TextView)findViewById(R.id.userIDEmail);
        emailTextView.setText("User ID: " + ModelManager.userEmail);

        final EditText passwordEditText = (EditText)findViewById(R.id.newPassword);
        final EditText confirmPasswordEditText = (EditText)findViewById(R.id.confirmNewPassword);
        Button updatePasswordBtn = (Button)findViewById(R.id.updatePasswordBtn);
        updatePasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passwordEditText.getText().toString().equals(confirmPasswordEditText.getText().toString())) {
                    AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
                    asyncHttpClient.get("http://f4xdigitalbillboards.com/app/api.php?cmd=changepassword&newpassword=" + passwordEditText.getText().toString() + "&token=" + ModelManager.userToken, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            displayAlert("Password updated!", "Your password has now been changed!", "Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    /*Handle okay clicked.*/
                                }
                            });
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                        }
                    });
                } else {
                    displayAlert("Whoops!", "Please double check the form above!", "Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            /*Handle okay clicked.*/
                        }
                    });
                }
            }
        });

        final ActionBarActivity actionBarActivity = this;
        Button logoutBtn = (Button)findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModelManager.userEmail = "";
                ModelManager.userToken = "";
                actionBarActivity.startActivity(new android.content.Intent(actionBarActivity.getApplicationContext(), Home.class));
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    void displayAlert(String title, String contents, String btnTitle, DialogInterface.OnClickListener listener){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AccountSettings.this);

        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(contents);
        alertDialogBuilder.setPositiveButton(btnTitle, listener);

        AlertDialog alertDialog = alertDialogBuilder.create();
        // show alert
        alertDialog.show();
    }
}
