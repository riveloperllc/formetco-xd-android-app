package com.formetco.formetcoxd;

import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.lucasr.twowayview.TwoWayView;

/**
 * Created by austinsweat on 7/12/15.
 */
public class XamplesExpanded extends ActionBarActivity {
    public static String xamplesType = "";
    public static Integer xamplesTypeCode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ModelManager.activitySetup(this, false);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); //Force Landscape mode for all devices.
        setContentView(R.layout.xamples_expanded_layout);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); //Force Landscape mode for all devices.

        TextView headerLabel = (TextView) findViewById(R.id.xamplesExpandedHeader);
        ModelManager.convertLabelToHNThin(this.getAssets(), headerLabel);
        headerLabel.setText(xamplesType);

        final AssetManager assetManager = this.getAssets();
        final LayoutInflater inflater = this.getLayoutInflater();
        final ActionBarActivity actionBarActivity = this;
        final TwoWayView billboardView = (TwoWayView) findViewById(R.id.xamplesExpandedTwoWayView);
        billboardView.setAdapter(new XamplesExpandedAdapter(billboardView.getWidth(), billboardView.getHeight(), inflater, assetManager, actionBarActivity, ModelManager.determineXample(xamplesTypeCode)));
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }
}

class XamplesExpandedAdapter extends BaseAdapter{

    private String folderName = "";
    private LayoutInflater inflater;
    private AssetManager AM;
    private ActionBarActivity actionBarActivity;
    private int cellWidth;
    private int cellHeight;

    public XamplesExpandedAdapter(int width, int height, LayoutInflater inflater, AssetManager AM, ActionBarActivity actionBarActivity, String folderName){
        this.folderName = folderName;
        this.inflater = inflater;
        this.AM = AM;
        this.actionBarActivity = actionBarActivity;
        this.cellWidth = width;
        this.cellHeight = height;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = this.inflater.inflate(R.layout.cell_xamples_billboard, parent, false);
        //RelativeLayout xpandedLayout = (RelativeLayout) convertView.findViewById(R.id.xamplesExpandedCellLayout);
        //xpandedLayout.setLayoutParams(new RelativeLayout.LayoutParams(this.cellWidth, this.cellHeight));

        ImageView xpandedImageView = (ImageView) convertView.findViewById(R.id.xamplesExpandedBillboardImageView);
        ModelManager.populateImageView(this.actionBarActivity, String.format("http://www.formetco.com/app/xamples/%s/%s.jpg", this.folderName, (position + 1)), xpandedImageView, null);
        return convertView;
    }
}