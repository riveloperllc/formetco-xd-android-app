package com.formetco.formetcoxd;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

/**
 * Created by austinsweat on 7/10/15.
 */
public class About extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_layout);
        ModelManager.activitySetup(this, false);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }
}
