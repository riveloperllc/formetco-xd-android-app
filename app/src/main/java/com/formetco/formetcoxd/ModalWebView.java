package com.formetco.formetcoxd;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by austinsweat on 7/10/15.
 */
public class ModalWebView extends ActionBarActivity {
    public static String url = "";
    private WebView modalWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modalwebview_layout);
        modalWebView = (WebView) findViewById(R.id.modalWebView);
        modalWebView.getSettings().setJavaScriptEnabled(true);
        modalWebView.setWebViewClient(new WebViewController());
        modalWebView.loadUrl(url);
        ModelManager.activitySetup(this, false);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        modalWebView.destroy();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        modalWebView.destroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        modalWebView.destroy();
    }
}

class WebViewController extends WebViewClient {

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }
}