package com.formetco.formetcoxd;

import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by austinsweat on 7/10/15.
 */
public class Xamples extends ActionBarActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ModelManager.isNetworkAvailable(this)) {
            ModelManager.activitySetup(this, false);
            if (ModelManager.isPhone(this)) {
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //Force Landscape mode for all devices.
            }
            setContentView(R.layout.xamples_layout);
            if (ModelManager.isPhone(this)) {
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //Force Landscape mode for all devices.
            }
        }else{
            ModelManager.activitySetup(this, false);
            setContentView(R.layout.missing_layout);
            TextView missingHeader = (TextView) findViewById(R.id.missingHeader);
            ModelManager.convertLabelToHNThin(this.getAssets(), missingHeader);
            TextView missingDescription = (TextView) findViewById(R.id.missingDescription);
            return;
        }

        try {
            if (ModelManager.isPhone(this)) {
                ListView xamplesListView = (ListView) findViewById(R.id.xamplesListView);
                xamplesListView.setAdapter(new XamplesAdapter(this.getAssets(), this, this.getLayoutInflater()));
                xamplesListView.setDivider(new ColorDrawable(Color.argb(0, 0, 0, 0)));
            } else {
                TextView headerLabel = (TextView) this.findViewById(R.id.xampleslabel);
                TextView descriptionLabel = (TextView) this.findViewById(R.id.xamplesdescription);

                ModelManager.convertLabelToHNThin(this.getAssets(), headerLabel);

                ImageButton xamplesTileOneBtn = (ImageButton) findViewById(R.id.xamplesTileOneBtn);
                xamplesTileOneBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Weather Conditions";
                        XamplesExpanded.xamplesTypeCode = 1;
                        startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                ImageButton xamplesTileTwoBtn = (ImageButton) findViewById(R.id.xamplesTileTwoBtn);
                xamplesTileTwoBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Live Data (RSS)";
                        XamplesExpanded.xamplesTypeCode = 2;
                        startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                ImageButton xamplesTileThreeBtn = (ImageButton) findViewById(R.id.xamplesTileThreeBtn);
                xamplesTileThreeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Countdowns";
                        XamplesExpanded.xamplesTypeCode = 3;
                        startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                ImageButton xamplesTileFourBtn = (ImageButton) findViewById(R.id.xamplesTileFourBtn);
                xamplesTileFourBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Photo Support";
                        XamplesExpanded.xamplesTypeCode = 4;
                        startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                ImageButton xamplesTileFiveBtn = (ImageButton) findViewById(R.id.xamplesTileFiveBtn);
                xamplesTileFiveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Time & Temp";
                        XamplesExpanded.xamplesTypeCode = 5;
                        startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                ImageButton xamplesTileSixBtn = (ImageButton) findViewById(R.id.xamplesTileSixBtn);
                xamplesTileSixBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Conditional Updates";
                        XamplesExpanded.xamplesTypeCode = 6;
                        startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                ImageButton xamplesTileSevenBtn = (ImageButton) findViewById(R.id.xamplesTileSevenBtn);
                xamplesTileSevenBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Social Media";
                        XamplesExpanded.xamplesTypeCode = 7;
                        startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                ImageButton xamplesTileEightBtn = (ImageButton) findViewById(R.id.xamplesTileEightBtn);
                xamplesTileEightBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Public Service";
                        XamplesExpanded.xamplesTypeCode = 8;
                        startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
            }
        }catch (Exception e){

        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }
}

class XamplesAdapter extends BaseAdapter {

    private AssetManager assetManager;
    private ActionBarActivity actionBarActivity;
    private LayoutInflater layoutInflater;
    private Object[] views;


    public XamplesAdapter(AssetManager assetManager, ActionBarActivity actionBarActivity, LayoutInflater layoutInflater){
        this.assetManager = assetManager;
        this.actionBarActivity = actionBarActivity;
        this.layoutInflater = layoutInflater;
        ArrayList<Integer> tmp_views = new ArrayList<Integer>();
        tmp_views.add(R.layout.cell_xamples_header_phone);
        tmp_views.add(R.layout.cell_xamples_phone);
        this.views = tmp_views.toArray();
    }

    @Override
    public int getCount() {
        return this.views.length;
    }

    @Override
    public Object getItem(int position) {
        return this.views[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = this.layoutInflater.inflate(((Integer)getItem(position)), parent, false);

        switch (position){
            case 0:
                TextView headerLabel = (TextView) convertView.findViewById(R.id.xampleslabel);
                TextView descriptionLabel = (TextView) convertView.findViewById(R.id.xamplesdescription);

                ModelManager.convertLabelToHNThin(this.assetManager, headerLabel);
                break;

            case 1:
                ImageButton xamplesTileOneBtn = (ImageButton) convertView.findViewById(R.id.xamplesTileOneBtn);
                xamplesTileOneBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Weather Conditions";
                        XamplesExpanded.xamplesTypeCode = 1;
                        actionBarActivity.startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                ImageButton xamplesTileTwoBtn = (ImageButton) convertView.findViewById(R.id.xamplesTileTwoBtn);
                xamplesTileTwoBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Live Data (RSS)";
                        XamplesExpanded.xamplesTypeCode = 2;
                        actionBarActivity.startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                ImageButton xamplesTileThreeBtn = (ImageButton) convertView.findViewById(R.id.xamplesTileThreeBtn);
                xamplesTileThreeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Countdowns";
                        XamplesExpanded.xamplesTypeCode = 3;
                        actionBarActivity.startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                ImageButton xamplesTileFourBtn = (ImageButton) convertView.findViewById(R.id.xamplesTileFourBtn);
                xamplesTileFourBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Photo Support";
                        XamplesExpanded.xamplesTypeCode = 4;
                        actionBarActivity.startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                ImageButton xamplesTileFiveBtn = (ImageButton) convertView.findViewById(R.id.xamplesTileFiveBtn);
                xamplesTileFiveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Time & Temp";
                        XamplesExpanded.xamplesTypeCode = 5;
                        actionBarActivity.startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                ImageButton xamplesTileSixBtn = (ImageButton) convertView.findViewById(R.id.xamplesTileSixBtn);
                xamplesTileSixBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Conditional Updates";
                        XamplesExpanded.xamplesTypeCode = 6;
                        actionBarActivity.startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                ImageButton xamplesTileSevenBtn = (ImageButton) convertView.findViewById(R.id.xamplesTileSevenBtn);
                xamplesTileSevenBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Social Media";
                        XamplesExpanded.xamplesTypeCode = 7;
                        actionBarActivity.startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                ImageButton xamplesTileEightBtn = (ImageButton) convertView.findViewById(R.id.xamplesTileEightBtn);
                xamplesTileEightBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XamplesExpanded.xamplesType = "Public Service";
                        XamplesExpanded.xamplesTypeCode = 8;
                        actionBarActivity.startActivity(new android.content.Intent(v.getContext(), XamplesExpanded.class));
                    }
                });
                break;

            default:
                break;
        }

        return convertView;
    }
}