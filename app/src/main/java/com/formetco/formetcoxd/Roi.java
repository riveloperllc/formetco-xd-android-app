package com.formetco.formetcoxd;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import at.grabner.circleprogress.CircleProgressView;

/**
 * Created by austinsweat on 1/25/16.
 */
public class Roi extends ActionBarActivity {
    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.roi_layout);
        ModelManager.activitySetup(this, false);

        EditText numberOfAdvertisers = (EditText) findViewById(R.id.bannerTextField1);
        EditText rateCardNetAmount = (EditText) findViewById(R.id.bannerTextField2);
        EditText discountPercent = (EditText) findViewById(R.id.bannerTextField3);
        EditText occupancyPercent = (EditText) findViewById(R.id.bannerTextField4);

        numberOfAdvertisers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        rateCardNetAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        discountPercent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        occupancyPercent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        EditText propertyLeaseAmount = (EditText) findViewById(R.id.expensebannerTextField1);
        EditText commissionPercent = (EditText) findViewById(R.id.expensebannerTextField2);
        EditText utilitiesAmount = (EditText) findViewById(R.id.expensebannerTextField3);
        EditText otherCostsAmount = (EditText) findViewById(R.id.expensebannerTextField4);

        propertyLeaseAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        commissionPercent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        utilitiesAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otherCostsAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        EditText digitalBillboardsAmount = (EditText) findViewById(R.id.constructionbannerTextField1);
        EditText structureAmount = (EditText) findViewById(R.id.constructionbannerTextField2);
        EditText otherConstructionCostsAmount = (EditText) findViewById(R.id.constructionbannerTextField3);

        digitalBillboardsAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        structureAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otherConstructionCostsAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContentView(R.layout.roi_layout);
        ModelManager.activitySetup(this, false);

        EditText numberOfAdvertisers = (EditText) findViewById(R.id.bannerTextField1);
        EditText rateCardNetAmount = (EditText) findViewById(R.id.bannerTextField2);
        EditText discountPercent = (EditText) findViewById(R.id.bannerTextField3);
        EditText occupancyPercent = (EditText) findViewById(R.id.bannerTextField4);

        numberOfAdvertisers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        rateCardNetAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        discountPercent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        occupancyPercent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        EditText propertyLeaseAmount = (EditText) findViewById(R.id.expensebannerTextField1);
        EditText commissionPercent = (EditText) findViewById(R.id.expensebannerTextField2);
        EditText utilitiesAmount = (EditText) findViewById(R.id.expensebannerTextField3);
        EditText otherCostsAmount = (EditText) findViewById(R.id.expensebannerTextField4);

        propertyLeaseAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        commissionPercent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        utilitiesAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otherCostsAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        EditText digitalBillboardsAmount = (EditText) findViewById(R.id.constructionbannerTextField1);
        EditText structureAmount = (EditText) findViewById(R.id.constructionbannerTextField2);
        EditText otherConstructionCostsAmount = (EditText) findViewById(R.id.constructionbannerTextField3);

        digitalBillboardsAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        structureAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otherConstructionCostsAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    void generate(){
        try {
            EditText numberOfAdvertisers = (EditText) findViewById(R.id.bannerTextField1);
            EditText rateCardNetAmount = (EditText) findViewById(R.id.bannerTextField2);
            EditText discountPercent = (EditText) findViewById(R.id.bannerTextField3);
            EditText occupancyPercent = (EditText) findViewById(R.id.bannerTextField4);

            EditText propertyLeaseAmount = (EditText) findViewById(R.id.expensebannerTextField1);
            EditText commissionPercent = (EditText) findViewById(R.id.expensebannerTextField2);
            EditText utilitiesAmount = (EditText) findViewById(R.id.expensebannerTextField3);
            EditText otherCostsAmount = (EditText) findViewById(R.id.expensebannerTextField4);

            EditText digitalBillboardsAmount = (EditText) findViewById(R.id.constructionbannerTextField1);
            EditText structureAmount = (EditText) findViewById(R.id.constructionbannerTextField2);
            EditText otherConstructionCostsAmount = (EditText) findViewById(R.id.constructionbannerTextField3);

            CircleProgressView monthlyEarningsProgressView = (CircleProgressView) findViewById(R.id.monthlyEarningsGraphView);
            CircleProgressView roiProgressView = (CircleProgressView) findViewById(R.id.roiGraphView);

            TextView monthlyEarningsLabel = (TextView) findViewById(R.id.bannerFooterResultLabel);
            TextView expensesEarningsLabel = (TextView) findViewById(R.id.expensesBannerResultLabel);
            TextView constructionsEarningsLabel = (TextView) findViewById(R.id.constructionBannerResultLabel);

            TextView earningsGraphLabel = (TextView) findViewById(R.id.earningsGraphLabel);
            TextView roiGraphLabel = (TextView) findViewById(R.id.roiGraphLabel);

            Double revenue = ((Double.parseDouble(numberOfAdvertisers.getText().toString()) * Double.parseDouble(rateCardNetAmount.getText().toString())) * (1 - (Double.parseDouble(discountPercent.getText().toString()) / 100))) * (Double.parseDouble(occupancyPercent.getText().toString()) / 100);
            Double expenses = (Double.parseDouble(propertyLeaseAmount.getText().toString()) + (((Double.parseDouble(commissionPercent.getText().toString()) / 100) * revenue)) + Double.parseDouble(utilitiesAmount.getText().toString()) + Double.parseDouble(otherCostsAmount.getText().toString()));
            Double constructionCosts = (Double.parseDouble(digitalBillboardsAmount.getText().toString()) + Double.parseDouble(structureAmount.getText().toString()) + Double.parseDouble(otherConstructionCostsAmount.getText().toString()));
            Double totalProfit = (revenue - expenses);
            Double yearsOfROI = ((constructionCosts / (revenue - expenses)) / 12);

            monthlyEarningsLabel.setText(String.format("$%.2f", revenue));
            expensesEarningsLabel.setText(String.format("$%.2f", expenses));
            constructionsEarningsLabel.setText(String.format("$%.2f", constructionCosts));
            earningsGraphLabel.setText(String.format("$%.2f", totalProfit));
            roiGraphLabel.setText(String.format("%.2f", yearsOfROI));

            Integer monthlyEarningsPercent = 50;
            Integer roiPercent = 50;

            monthlyEarningsProgressView.setValueAnimated(monthlyEarningsPercent);
            roiProgressView.setValueAnimated(roiPercent);
        }catch (Exception e){
            return;
        }
    }
}
