package com.formetco.formetcoxd;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.FloatMath;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import org.lucasr.twowayview.TwoWayView;

/**
 * Created by austinsweat on 7/17/15.
 */
public class Xpand extends ActionBarActivity {
    static final int REQUEST_IMAGE_CAPTURE = 1; //Default

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ModelManager.isPhone(this)) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //Force Landscape mode for all devices.
        }
        setContentView(com.formetco.formetcoxd.R.layout.xpand_layout);
        if (ModelManager.isPhone(this)) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //Force Landscape mode for all devices.
        }
        activitySetup(this, false);

        try {
            ImageButton cameraBtn = (ImageButton) findViewById(com.formetco.formetcoxd.R.id.xpandCameraBtn);
            cameraBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
            });

            ImageButton galleryBtn = (ImageButton) findViewById(com.formetco.formetcoxd.R.id.xpandGalleryBtn);
            galleryBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
                    startActivityForResult(galleryIntent, REQUEST_IMAGE_CAPTURE);
                }
            });


            final AbsoluteLayout environmentLayout = (AbsoluteLayout) findViewById(com.formetco.formetcoxd.R.id.xpandEnvironmentOverlay);

            environmentLayout.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getPointerCount() == 5) {
                        environmentLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                        environmentLayout.setVisibility(View.INVISIBLE);
                    }

                    return false;
                }
            });

            final View v = (View) findViewById(com.formetco.formetcoxd.R.id.xpandEnvironmentOverlay2);

            final TwoWayView gridView = (TwoWayView) findViewById(com.formetco.formetcoxd.R.id.xpandDragTable);
            gridView.setAdapter(new XpandAdapter(this.getLayoutInflater(), this, this.getApplicationContext(), environmentLayout, environmentLayout));

            final ImageButton shareBtn = (ImageButton) findViewById(com.formetco.formetcoxd.R.id.xpandShareBtn);
            shareBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //View view = findViewById(R.id.layout);//your layout id
                    saved = true;
                    final TextView savedNotification = (TextView) findViewById(R.id.xpandSavedNotification);
                    savedNotification.setVisibility(View.VISIBLE);

                    Thread thread = new Thread(){
                        @Override
                        public void run() {
                            try {
                                synchronized(this){
                                    wait(4000);
                                }
                            }
                            catch(InterruptedException ex){ }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    savedNotification.setVisibility(View.INVISIBLE);
                                }
                            });
                        }
                    };

                    thread.start();

                    MediaPlayer mp = MediaPlayer.create(Xpand.this, R.raw.app);
                    mp.start();
                    Vibrator v2 = (Vibrator) Xpand.this.getSystemService(Context.VIBRATOR_SERVICE);
                    v2.vibrate(500);

                    environmentLayout.getRootView();
                    String state = Environment.getExternalStorageState();


                    if (Environment.MEDIA_MOUNTED.equals(state)) {
                        File picDir = new File(Environment.getExternalStorageDirectory() + "/formetco");
                        if (!picDir.exists()) {
                            picDir.mkdir();
                        }
                        environmentLayout.setDrawingCacheEnabled(true);
                        environmentLayout.buildDrawingCache(true);
                        Bitmap bitmap = environmentLayout.getDrawingCache();
//          Date date = new Date();
                        String fileName = "xpand" + ".jpg";
                        File picFile = new File(picDir + "/" + fileName);
                        try {
                            picFile.createNewFile();
                            FileOutputStream picOut = new FileOutputStream(picFile);
                            addImageToGallery(picFile.getPath(), getApplicationContext());
                            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), (int) (bitmap.getHeight() / 1.2));
                            boolean saved = bitmap.compress(Bitmap.CompressFormat.JPEG, 100, picOut);
                            if (saved) {
                                Toast.makeText(getApplicationContext(), "Image saved to your device Pictures " + "directory!", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Could not save image to your device.", Toast.LENGTH_LONG).show();
                            }
                            picOut.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        environmentLayout.destroyDrawingCache();
                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                        sharingIntent.setType("image/jpeg");
                        sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(picFile.getAbsolutePath()));
                        startActivity(Intent.createChooser(sharingIntent, "Share via"));
                    } else {
                        //Error
                        Log.e("Error", "with image");
                    }
                }
            });


            final TextView headerLabel = (TextView) findViewById(com.formetco.formetcoxd.R.id.xpandLabel);
            ModelManager.convertLabelToHNThin(getAssets(), headerLabel);
        }catch (NullPointerException e){

        }
    }

    public static void addImageToGallery(final String filePath, final Context context) {

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filePath);

        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            try {
                Bundle extras = data.getExtras();
                Bitmap photoTakenBitmap = (Bitmap) extras.get("data");

                AbsoluteLayout environmentOverlay = (AbsoluteLayout) findViewById(com.formetco.formetcoxd.R.id.xpandEnvironmentOverlay);
                environmentOverlay.setBackgroundDrawable(new BitmapDrawable(photoTakenBitmap));
                environmentOverlay.setVisibility(View.VISIBLE);

            }catch (Exception e){
                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();
                AbsoluteLayout environmentOverlay = (AbsoluteLayout) findViewById(com.formetco.formetcoxd.R.id.xpandEnvironmentOverlay);
                environmentOverlay.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeFile(imgDecodableString)));
                environmentOverlay.setVisibility(View.VISIBLE);
            }

            final ImageButton shareBtn = (ImageButton) findViewById(com.formetco.formetcoxd.R.id.xpandShareBtn);
            shareBtn.setBackgroundColor(Color.argb(255, 173, 23, 43));
            shareBtn.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.icon_share_clicked));
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("STOP", "CALLED");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("STOPING", "CALLED");
    }

    public static boolean saved = false;

    public static void activitySetup(final ActionBarActivity actionBarActivity, final boolean isHome){
        final AbsoluteLayout environmentLayout = (AbsoluteLayout) actionBarActivity.findViewById(com.formetco.formetcoxd.R.id.xpandEnvironmentOverlay);

        if(!actionBarActivity.getResources().getBoolean(R.bool.portrait_only)){
            actionBarActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            actionBarActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        ActionBar actionBar = actionBarActivity.getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        actionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater layoutInflater = actionBarActivity.getLayoutInflater();
        View actionbar_view = layoutInflater.inflate(R.layout.action_bar, null);

        ImageButton leftDrawerBtn = (ImageButton) actionbar_view.findViewById(R.id.leftDrawerBtn);

        DrawerLayout leftDrawerLayout = null;
        android.support.v4.app.ActionBarDrawerToggle leftDrawerToggle = null;
        ListView leftDrawerListView = null;

        if(!isHome){
            leftDrawerBtn.setImageDrawable(actionBarActivity.getResources().getDrawable(R.drawable.icon_back_arrow));
        }else{
            leftDrawerLayout = (DrawerLayout) actionBarActivity.findViewById(R.id.drawerLayout);
            leftDrawerToggle = new android.support.v4.app.ActionBarDrawerToggle(actionBarActivity, leftDrawerLayout, true, R.drawable.icon_hamburger, R.string.drawer_open, R.string.drawer_close){
                @Override
                public void onDrawerClosed(View drawerView){
                    actionBarActivity.invalidateOptionsMenu();
                }

                @Override
                public void onDrawerOpened(View drawerView){
                    actionBarActivity.invalidateOptionsMenu();
                }

                public boolean onCreateOptionsMenu(Menu menu){
                    return true; /*Default*/
                }
            };

            final android.support.v4.app.ActionBarDrawerToggle tmp_Toggle = leftDrawerToggle;
            leftDrawerListView = (ListView) actionBarActivity.findViewById(R.id.navigationList);
            leftDrawerListView.setAdapter(new NavigationAdapter(actionBarActivity.getAssets(), actionBarActivity, actionBarActivity.getLayoutInflater()));

            leftDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    tmp_Toggle.syncState();
                }
            });

            leftDrawerLayout.setDrawerListener(leftDrawerToggle);
        }

        final DrawerLayout tmp_DrawerLayout = leftDrawerLayout;
        final android.support.v4.app.ActionBarDrawerToggle tmp_Toggle = leftDrawerToggle;

        leftDrawerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isHome){
                    if (saved || (environmentLayout.getVisibility() != View.VISIBLE)) {
                        actionBarActivity.finish();
                    }else{
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(actionBarActivity);
                        alertDialog.setTitle("X-it without saving?");
                        alertDialog.setMessage("Please make sure not to leave your masterpiece behnid!");
                        alertDialog.setCancelable(true);
                        alertDialog.setPositiveButton("Leave",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        actionBarActivity.finish();
                                    }
                                });

                        alertDialog.setNegativeButton("Stay", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        AlertDialog alertDialog1 = alertDialog.create();
                        alertDialog1.show();
                    }
                }else{
                    //Open Sidebar
                    if (tmp_DrawerLayout.isDrawerOpen(Gravity.LEFT)){
                        tmp_DrawerLayout.closeDrawer(Gravity.LEFT);
                        tmp_Toggle.syncState();
                    }else{
                        tmp_DrawerLayout.openDrawer(Gravity.LEFT);
                        tmp_Toggle.syncState();
                    }
                }
            }
        });

        ImageButton logoBtn = (ImageButton) actionbar_view.findViewById(R.id.logoBtn);
        logoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionBarActivity.finish();
                actionBarActivity.startActivity(new Intent(actionBarActivity, Home.class));
            }
        });

        ImageButton rightDrawerBtn = (ImageButton) actionbar_view.findViewById(R.id.rightDrawerBtn);
        rightDrawerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ModelManager.isPhone(actionBarActivity)){
                    //Call
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(actionBarActivity);
                    alertDialog.setTitle("Contact Us");
                    alertDialog.setMessage("Dial +1 (800) 367-6382?");
                    alertDialog.setCancelable(true);
                    alertDialog.setPositiveButton("Call",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(Intent.ACTION_DIAL);
                                    intent.setData(Uri.parse("tel:18003676382"));
                                    actionBarActivity.startActivity(intent);
                                }
                            });

                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog alertDialog1 = alertDialog.create();
                    alertDialog1.show();
                }else{
                    //Open Xpert
                    actionBarActivity.startActivity(new android.content.Intent(actionBarActivity.getApplicationContext(), Xpert.class));
                }
            }
        });

        actionBar.setCustomView(actionbar_view);
        actionBar.setDisplayShowCustomEnabled(true);
    }
}

class XpandAdapter extends BaseAdapter {

    private ActionBarActivity actionBarActivity;
    private Context context;
    private LayoutInflater inflater;
    private Object[] items;
    private AbsoluteLayout environmentOverlay;
    private View environmentOverlay2;
    private ImageView activeImageView; //Null ImageView;

    public XpandAdapter(LayoutInflater inflater, ActionBarActivity actionBarActivity, Context context, View e, AbsoluteLayout environmentOverlay){
        this.actionBarActivity = actionBarActivity;
        this.context = context;
        this.inflater = inflater;
        this.environmentOverlay = environmentOverlay;
        this.environmentOverlay2 = e;
        ArrayList<Integer> tmp = new ArrayList<>();
        tmp.add(com.formetco.formetcoxd.R.drawable.xpand_board1);
        tmp.add(com.formetco.formetcoxd.R.drawable.xpand_board2);
        tmp.add(com.formetco.formetcoxd.R.drawable.xpand_board3);
        tmp.add(com.formetco.formetcoxd.R.drawable.xpand_board4);
        tmp.add(com.formetco.formetcoxd.R.drawable.xpand_board5);
        tmp.add(com.formetco.formetcoxd.R.drawable.xpand_board6);
        tmp.add(com.formetco.formetcoxd.R.drawable.xpand_board7);
        tmp.add(com.formetco.formetcoxd.R.drawable.xpand_board8);
        tmp.add(com.formetco.formetcoxd.R.drawable.xpand_board9);
        tmp.add(com.formetco.formetcoxd.R.drawable.xpand_board10);
        tmp.add(com.formetco.formetcoxd.R.drawable.xpand_board11);
        tmp.add(com.formetco.formetcoxd.R.drawable.xpand_board12);
        tmp.add(com.formetco.formetcoxd.R.drawable.xpand_board13);
        tmp.add(com.formetco.formetcoxd.R.drawable.xpand_board14);
        this.items = tmp.toArray();
        this.activeImageView = new ImageView(this.context);
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static View oldView;
    static Boolean oldBool = false;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(com.formetco.formetcoxd.R.layout.cell_xpand_draggable, parent, false);
        final ImageView itemImageView = (ImageView) convertView.findViewById(R.id.xpandDraggableImageView);
        itemImageView.setImageBitmap(BitmapFactory.decodeResource(actionBarActivity.getResources(), (int)items[position]));
        View.OnLongClickListener cellLongClicked; //Double clicked from bottom port to show at (0,0) of environment overlay.
        View.OnTouchListener cellTouched;
        final View.OnDragListener cellDragged;

        cellDragged = new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                if (environmentOverlay.getVisibility() == View.INVISIBLE) { return false; }
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        Log.d("drag", "started");
                        //oldBool = false;
                        break;
                    case DragEvent.ACTION_DRAG_ENTERED:
                        Log.d("drag", "entered");
                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        Log.d("drag", "exited");
                        break;
                    case DragEvent.ACTION_DROP:
                        Log.d("drag", "drop");
                        Log.d("drag", "Added view");
                        AbsoluteLayout.LayoutParams params = new AbsoluteLayout.LayoutParams(400, 200, (int)event.getX(), (int)event.getY());
                        activeImageView = new ImageView(context);
                        activeImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        activeImageView.setImageBitmap(((BitmapDrawable) itemImageView.getDrawable()).getBitmap());
                        //activeImageView.setBackgroundColor(Color.CYAN); //Debug
                        activeImageView.setLayoutParams(params);
                        addGestures(environmentOverlay, activeImageView);
                        addBorder(activeImageView);
                        environmentOverlay.addView(activeImageView, params);
                        break;
                    case DragEvent.ACTION_DRAG_ENDED:
                        Log.d("drag", "ended");

                        break;
                    default:
                        break;
                }
                return true;
            }
        };

        cellTouched = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (environmentOverlay.getVisibility() == View.INVISIBLE) { return false; }
                    environmentOverlay.setOnDragListener(cellDragged);
                    selectedImageView = (ImageView) v;
                    View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                    v.startDrag(null, shadowBuilder, v, 0);
                    v.setVisibility(View.VISIBLE);
                    return true;
                }else{
                    return false;
                }
            }
        };


        itemImageView.setOnTouchListener(cellTouched); //Init drag gesture, overridden after this one.
        //itemImageView.setOnLongClickListener(cellLongClicked); //Init drag gesture, overridden after this one.
        //environmentOverlay.setOnDragListener(cellDragged);
        return convertView;
    }

    Paint borderPaint = new Paint();
    Canvas borderCanvas = new Canvas();
    void addBorder(ImageView imageView){
        removeBorders();

        float posX = imageView.getX();
        float posY = imageView.getY();
        float width = imageView.getWidth();
        float height = imageView.getHeight();


        //Add activeimage border;
    }

    void removeBorders(){
        //Remove activeimage border;

    }

    ImageView selectedImageView;
    // These matrices will be used to move and zoom image
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();
    String TAG = "TAG";
    // We can be in one of these 3 states
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    // Remember some things for zooming
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;
    String savedItemClicked;

    void addGestures(final View view, final ImageView imageView){
        View.OnTouchListener viewMoved;
        View.OnDragListener viewDragged;
        final ScaleGestureDetector viewScaled = new ScaleGestureDetector(actionBarActivity, new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                return false;
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                return false;
            }

            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {

            }
        });

        activeImageView = imageView;
        viewMoved = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                ImageView view = (ImageView) v;
                dumpEvent(event);

                // Handle touch events here...
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        savedMatrix.set(matrix);
                        start.set(event.getX(), event.getY());
                        Log.d(TAG, "mode=DRAG");
                        mode = DRAG;
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        oldDist = spacing(event);
                        Log.d(TAG, "oldDist=" + oldDist);
                        if (oldDist > 10f) {
                            savedMatrix.set(matrix);
                            midPoint(mid, event);
                            mode = ZOOM;
                            Log.d(TAG, "mode=ZOOM");
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_POINTER_UP:
                        mode = NONE;
                        Log.d(TAG, "mode=NONE");
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (mode == ZOOM) {
                            float newDist = spacing(event);
                            Log.d(TAG, "newDist=" + newDist);
                            if (newDist > 10f) {
                                matrix.set(savedMatrix);
                                float scale = newDist / oldDist;
                                matrix.postScale(scale, scale, mid.x, mid.y);
                            }
                        }
                        break;
                }

                view.setImageMatrix(matrix);


                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        Log.d("Touch", "Down");
                        activeImageView = imageView;
                        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                        v.startDrag(null, shadowBuilder, v, 0);
                        break;

                    case MotionEvent.ACTION_UP:
                        Log.d("Touch", "Up");
                        break;

                    case MotionEvent.ACTION_MOVE:
                        Log.d("Touch", "Move");
                        break;

                    case MotionEvent.ACTION_MASK:
                        Log.d("Touch", "Mask");
                        break;

                    default:
                        //v.setVisibility(View.VISIBLE); //Default action
                        break;
                }
                return true;
            }
        };

        viewDragged = new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        Log.d("drag", "started");
                        activeImageView.setVisibility(View.INVISIBLE);
                        break;
                    case DragEvent.ACTION_DRAG_ENTERED:
                        Log.d("drag", "entered");
                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        Log.d("drag", "exited");
                        break;
                    case DragEvent.ACTION_DROP:
                        Log.d("drag", "drop");
                        activeImageView.setX(event.getX());
                        activeImageView.setY(event.getY());
                        Log.d("X/Y", String.format("%s/%s", event.getX(), event.getY()));
                        activeImageView.setVisibility(View.VISIBLE);
                        break;
                    case DragEvent.ACTION_DRAG_ENDED:
                        Log.d("drag", "ended");
                        activeImageView.setVisibility(View.VISIBLE);
                        break;
                    default:
                        break;
                }

                return true;
            }
        };

        imageView.setOnTouchListener(viewMoved);
        environmentOverlay2.setOnDragListener(viewDragged);
    }

    private void dumpEvent(MotionEvent event) {
        String names[] = { "DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE",
                "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?" };
        StringBuilder sb = new StringBuilder();
        int action = event.getAction();
        int actionCode = action & MotionEvent.ACTION_MASK;
        sb.append("event ACTION_").append(names[actionCode]);
        if (actionCode == MotionEvent.ACTION_POINTER_DOWN
                || actionCode == MotionEvent.ACTION_POINTER_UP) {
            sb.append("(pid ").append(
                    action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
            sb.append(")");
        }
        sb.append("[");
        for (int i = 0; i < event.getPointerCount(); i++) {
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i + 1 < event.getPointerCount())
                sb.append(";");
        }
        sb.append("]");
        Log.d(TAG, sb.toString());
    }

    /** Determine the space between the first two fingers */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return FloatMath.sqrt(x * x + y * y);
    }

    /** Calculate the mid point of the first two fingers */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }
}

