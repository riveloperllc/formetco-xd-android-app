package com.formetco.formetcoxd;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ImageButton;

/**
 * Created by austinsweat on 7/8/15.
 */
public class Home extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

        setContentView(R.layout.home_layout);
        ModelManager.activitySetup(this, true);

        ImageButton xplainBtn = (ImageButton) findViewById(R.id.xplainbtn);
        xplainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Xplain.class));
            }
        });

        ImageButton xtrasBtn = (ImageButton) findViewById(R.id.xtrasbtn);
        xtrasBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Xtras.class));
            }
        });

        ImageButton xploreBtn = (ImageButton) findViewById(R.id.xplorebtn);
        final ActionBarActivity tmpActivity = this;
        xploreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(tmpActivity);
                alertDialog.setTitle("Confirm");
                alertDialog.setMessage("You are about to navigate out of this app to http://www.formetco.com/ftx");
                alertDialog.setCancelable(true);
                alertDialog.setPositiveButton("Confirm",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse("http://www.formetco.com/ftx"));
                                tmpActivity.startActivity(intent);
                            }
                        });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog1 = alertDialog.create();
                alertDialog1.show();
            }
        });

        ImageButton xamplesBtn = (ImageButton) findViewById(R.id.xamplesbtn);
        xamplesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Xamples.class));
            }
        });

        ImageButton xpertBtn = (ImageButton) findViewById(R.id.xpertbtn);
        xpertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Xpert.class));
            }
        });

        ImageButton xperienceBtn = (ImageButton) findViewById(R.id.xperiencebtn);
        ImageButton xamineBtn = (ImageButton) findViewById(R.id.xaminebtn);
        xamineBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Xamine.class));
            }
        });

        ImageButton xpandBtn = (ImageButton) findViewById(R.id.xpandbtn);
        xpandBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Xpand.class));
            }
        });

        ImageButton roiBtn = (ImageButton) findViewById(R.id.roiButton);
        roiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Roi.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContentView(R.layout.home_layout);

        ModelManager.activitySetup(this, true);

        ImageButton xplainBtn = (ImageButton) findViewById(R.id.xplainbtn);
        xplainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Xplain.class));
            }
        });

        ImageButton xtrasBtn = (ImageButton) findViewById(R.id.xtrasbtn);
        xtrasBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Xtras.class));
            }
        });

        ImageButton xploreBtn = (ImageButton) findViewById(R.id.xplorebtn);
        final ActionBarActivity tmpActivity = this;
        xploreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(tmpActivity);
                alertDialog.setTitle("Confirm");
                alertDialog.setMessage("You are about to navigate out of this app to http://www.formetco.com/ftx");
                alertDialog.setCancelable(true);
                alertDialog.setPositiveButton("Confirm",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse("http://www.formetco.com/ftx"));
                                tmpActivity.startActivity(intent);
                            }
                        });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog1 = alertDialog.create();
                alertDialog1.show();
            }
        });

        ImageButton xamplesBtn = (ImageButton) findViewById(R.id.xamplesbtn);
        xamplesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Xamples.class));
            }
        });

        ImageButton xpertBtn = (ImageButton) findViewById(R.id.xpertbtn);
        xpertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Xpert.class));
            }
        });

        ImageButton xperienceBtn = (ImageButton) findViewById(R.id.xperiencebtn);
        xperienceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Xperience.class));
            }
        });

        ImageButton xamineBtn = (ImageButton) findViewById(R.id.xaminebtn);
        xamineBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Xamine.class));
            }
        });

        ImageButton xpandBtn = (ImageButton) findViewById(R.id.xpandbtn);
        xpandBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Xpand.class));
            }
        });

        ImageButton roiBtn = (ImageButton) findViewById(R.id.roiButton);
        roiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(v.getContext(), Roi.class));
            }
        });
    }
}
