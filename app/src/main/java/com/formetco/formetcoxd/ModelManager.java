package com.formetco.formetcoxd;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

/**
 * Created by austinsweat on 7/7/15.
 */
public class ModelManager {

    public static String userToken = "";
    public static String userEmail = "";

    public static String xperience_EventType = "Formetco Campus Tour";
    public static String xperience_ContactName = "";
    public static String xperience_Email = "";
    public static String xperience_Phone = "";
    public static String xperience_Org = "";
    public static int xperience_Year = 2015;
    public static int xperience_Month = 1;
    public static int xperience_Day = 1;
    public static int xperience_Hour = 1;
    public static int xperience_Minute = 30;
    public static int xperience_TimeOfDay = 2;

    public static String xpert_Name = "";
    public static String xpert_Org = "";
    public static String xpert_Phone = "";
    public static String xpert_Email = "";
    public static String xpert_Questions = "";

    public static void activitySetup(final ActionBarActivity actionBarActivity, final boolean isHome){
        if(!actionBarActivity.getResources().getBoolean(R.bool.portrait_only)){
            actionBarActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            actionBarActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        ActionBar actionBar = actionBarActivity.getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        actionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater layoutInflater = actionBarActivity.getLayoutInflater();
        View actionbar_view = layoutInflater.inflate(R.layout.action_bar, null);

        ImageButton leftDrawerBtn = (ImageButton) actionbar_view.findViewById(R.id.leftDrawerBtn);

        DrawerLayout leftDrawerLayout = null;
        android.support.v4.app.ActionBarDrawerToggle leftDrawerToggle = null;
        ListView leftDrawerListView = null;

        if(!isHome){
            leftDrawerBtn.setImageDrawable(actionBarActivity.getResources().getDrawable(R.drawable.icon_back_arrow));
        }else{
            leftDrawerLayout = (DrawerLayout) actionBarActivity.findViewById(R.id.drawerLayout);
            leftDrawerToggle = new android.support.v4.app.ActionBarDrawerToggle(actionBarActivity, leftDrawerLayout, true, R.drawable.icon_hamburger, R.string.drawer_open, R.string.drawer_close){
                @Override
                public void onDrawerClosed(View drawerView){
                    actionBarActivity.invalidateOptionsMenu();
                }

                @Override
                public void onDrawerOpened(View drawerView){
                    actionBarActivity.invalidateOptionsMenu();
                }

                public boolean onCreateOptionsMenu(Menu menu){
                    return true; /*Default*/
                }
            };

            final android.support.v4.app.ActionBarDrawerToggle tmp_Toggle = leftDrawerToggle;
            leftDrawerListView = (ListView) actionBarActivity.findViewById(R.id.navigationList);
            leftDrawerListView.setAdapter(new NavigationAdapter(actionBarActivity.getAssets(), actionBarActivity, actionBarActivity.getLayoutInflater()));

            leftDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    tmp_Toggle.syncState();
                }
            });

            leftDrawerLayout.setDrawerListener(leftDrawerToggle);
        }

        final DrawerLayout tmp_DrawerLayout = leftDrawerLayout;
        final android.support.v4.app.ActionBarDrawerToggle tmp_Toggle = leftDrawerToggle;

        leftDrawerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isHome){
                    actionBarActivity.finish();
                }else{
                    //Open Sidebar
                    if (tmp_DrawerLayout.isDrawerOpen(Gravity.LEFT)){
                        tmp_DrawerLayout.closeDrawer(Gravity.LEFT);
                        tmp_Toggle.syncState();
                    }else{
                        tmp_DrawerLayout.openDrawer(Gravity.LEFT);
                        tmp_Toggle.syncState();
                    }
                }
            }
        });

        ImageButton logoBtn = (ImageButton) actionbar_view.findViewById(R.id.logoBtn);
        logoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionBarActivity.finish();
                actionBarActivity.startActivity(new Intent(actionBarActivity, Home.class));
            }
        });

        ImageButton rightDrawerBtn = (ImageButton) actionbar_view.findViewById(R.id.rightDrawerBtn);
        rightDrawerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPhone(actionBarActivity)){
                    //Call
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(actionBarActivity);
                    alertDialog.setTitle("Contact Us");
                    alertDialog.setMessage("Dial +1 (800) 367-6382?");
                    alertDialog.setCancelable(true);
                    alertDialog.setPositiveButton("Call",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(Intent.ACTION_DIAL);
                                    intent.setData(Uri.parse("tel:18003676382"));
                                    actionBarActivity.startActivity(intent);
                                }
                            });

                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog alertDialog1 = alertDialog.create();
                    alertDialog1.show();
                }else{
                    //Open Xpert
                    actionBarActivity.startActivity(new android.content.Intent(actionBarActivity.getApplicationContext(), Xpert.class));
                }
            }
        });

        actionBar.setCustomView(actionbar_view);
        actionBar.setDisplayShowCustomEnabled(true);
    }

    public static boolean isLoggedIn(){
        if (ModelManager.userEmail != "" && ModelManager.userToken != ""){
            return true;
        }else{
            return false;
        }
    }

    public static String determineXample(Integer id){
        switch (id){
            case 1:
                return "weather_forecasts";

            case 2:
                return "live_data";

            case 3:
                return "timed_countdowns";

            case 4:
                return "photo_support";

            case 5:
                return "time_and_temp";

            case 6:
                return "conditional_update";

            case 7:
                return "social_media";

            case 8:
                return "public_service";

            default:
                return "blank";

        }
    }

    public static boolean isNetworkAvailable(ActionBarActivity actionBarActivity) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) actionBarActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isPhone(ActionBarActivity actionBarActivity){
        if (actionBarActivity.getResources().getBoolean(R.bool.phone)){
            return true;
        }else{
            return false;
        }
    }

    public static void convertLabelToHNThin(AssetManager AM, TextView textViewObject){
        try{
            textViewObject.setTypeface(Typeface.createFromAsset(AM, "fonts/HN_Thin.ttf"));
        }catch (Exception e){ }
    }

    public static void convertLabelToHNLight(AssetManager AM, TextView textViewObject){
        try {
            textViewObject.setTypeface(Typeface.createFromAsset(AM, "fonts/HN_LtEX.ttf"));
        }catch (Exception e){ }
    }

    public static void convertButtonToHNThin(AssetManager AM, Button btnObject){
        try {
            btnObject.setTypeface(Typeface.createFromAsset(AM, "fonts/HN_Thin.ttf"));
        }catch (Exception e){ }
    }

    public static void convertButtonToHNLight(AssetManager AM, Button btnObject){
        try {
            btnObject.setTypeface(Typeface.createFromAsset(AM, "fonts/rb_light.ttf"));
        }catch (Exception e){ }
    }

    public static void populateImageView(final ActionBarActivity activity, final String url, final ImageView imageView, final ProgressBar progressBar){
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (progressBar != null) {
                    progressBar.setActivated(false);
                    progressBar.setVisibility(View.INVISIBLE);
                }

                try {
                    //Replaced with downsizing method below.
                    //Bitmap bytesBitmap = BitmapFactory.decodeByteArray(responseBody, 0, responseBody.length);
                    imageView.setImageBitmap(decodeSampledBitmapFromByteArray(responseBody, imageView.getWidth(), imageView.getHeight()));

                }catch (Exception e){
                    Log.e("Error", e.getMessage());
                    Toast.makeText(activity, "Something went wrong when displaying image, retrying...", Toast.LENGTH_SHORT);
                    populateImageView(activity, url, imageView, progressBar);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(activity, "Something went wrong when displaying image, retrying...", Toast.LENGTH_SHORT);
                populateImageView(activity, url, imageView, progressBar);
            }
        });
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static Bitmap decodeSampledBitmapFromByteArray(byte[] data, int reqWidth, int reqHeight){
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data, 0, data.length, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);

        return bitmap;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

}

class Model_Xample {
    public int ID;
    public String folderName;
    public String properName;

    public Model_Xample(int id, String folder, String name){
        this.ID = id;
        this.folderName = folder;
        this.properName = name;
    }
}

class Model_Xtra {
    public String fileName;
    public String properName;
    public Boolean locked;

    public Model_Xtra(String file, String name, Boolean locked){
        this.fileName = file;
        this.properName = name;
        this.locked = locked;
    }
}

class Model_Xplain {
    public int ID;
    public String youtubeTitle;
    public String youtubeDescription;
    public String youtubeID;

    public Model_Xplain(int id, String title, String description, String ytID){
        this.ID = id;
        this.youtubeTitle = title;
        this.youtubeDescription = description;
        this.youtubeID = ytID;
    }
}

class Model_XplainPage {
    public int ID;
    public Model_Xplain[] modelXplainYoutubeVideos;

    public Model_XplainPage(int id, Model_Xplain[] videos){
        this.ID = id;
        this.modelXplainYoutubeVideos = videos;
    }
}

class NavigationAdapter extends BaseAdapter {
    private String[] loggedInList = {"Home", "Settings", "About", "Logout"};
    private String[] loggedOutList = {"Home", "Login", "About"};
    private AssetManager assetManager;
    private ActionBarActivity actionBarActivity;
    private LayoutInflater layoutInflater;

    public NavigationAdapter(AssetManager assetManager, ActionBarActivity actionBarActivity, LayoutInflater layoutInflater){
        this.assetManager = assetManager;
        this.actionBarActivity = actionBarActivity;
        this.layoutInflater = layoutInflater;
    }

    @Override
    public int getCount() {

        if (ModelManager.isLoggedIn()) {
            return loggedInList.length;
        } else {
            return loggedOutList.length;
        }
    }

    @Override
    public Object getItem(int position) {
        if (ModelManager.isLoggedIn()) {
            return loggedInList[position];
        } else {
            return loggedOutList[position];
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = this.layoutInflater.inflate(R.layout.cell_drawer, parent, false);
        TextView drawerLabel = (TextView) convertView.findViewById(R.id.drawerCellLabel);
        ModelManager.convertLabelToHNThin(this.assetManager, drawerLabel);
        if (ModelManager.isLoggedIn()) {
            drawerLabel.setText(loggedInList[position]);
        }else{
            drawerLabel.setText(loggedOutList[position]);
        }

        drawerLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(position){
                    case 0:
                        actionBarActivity.startActivity(new android.content.Intent(actionBarActivity.getApplicationContext(), Home.class));
                        break;

                    case 1:
                        if (ModelManager.isLoggedIn()){
                            actionBarActivity.startActivity(new android.content.Intent(actionBarActivity.getApplicationContext(), AccountSettings.class));
                        }else{
                            actionBarActivity.startActivity(new android.content.Intent(actionBarActivity.getApplicationContext(), Login.class));
                        }
                        break;

                    case 2:
                        actionBarActivity.startActivity(new android.content.Intent(actionBarActivity.getApplicationContext(), About.class));
                        break;

                    case 3:
                        //logout
                        ModelManager.userEmail = "";
                        ModelManager.userToken = "";
                        actionBarActivity.startActivity(new android.content.Intent(actionBarActivity.getApplicationContext(), Home.class));
                        break;
                }
            }
        });
        return convertView;
    }
}
