package com.formetco.formetcoxd;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;

/**
 * Created by austinsweat on 7/9/15.
 */
public class Xplain extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (ModelManager.isNetworkAvailable(this)) {
                if (ModelManager.isPhone(this)) {
                    this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //Force Landscape mode for all devices.
                }
                setContentView(R.layout.xplain_layout);
                if (ModelManager.isPhone(this)) {
                    this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //Force Landscape mode for all devices.
                }
                ModelManager.activitySetup(this, false);
            } else {
                setContentView(R.layout.missing_layout);
                ModelManager.activitySetup(this, false);
                TextView missingHeader = (TextView) findViewById(R.id.missingHeader);
                ModelManager.convertLabelToHNThin(this.getAssets(), missingHeader);
                TextView missingDescription = (TextView) findViewById(R.id.missingDescription);
                return;
            }

            if (ModelManager.isPhone(this)) {
                ListView xplainListView = (ListView) findViewById(R.id.xplainListView);
                xplainListView.setAdapter(
                        new XplainAdapter(
                                this.getAssets(),
                                this.getLayoutInflater(),
                                this.getApplicationContext(),
                                this
                        )
                );

                xplainListView.setDivider(new ColorDrawable(Color.argb(0, 0, 0, 0)));
            } else {
                TwoWayView xplainTwoWayGridView = (TwoWayView) findViewById(R.id.xplainTwoWayView);
                xplainTwoWayGridView.setAdapter(new XplainTabletAdapter(
                                this.getAssets(),
                                this.getLayoutInflater(),
                                this.getApplicationContext(),
                                this
                        )
                );
            }
        }catch (Exception e){

        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }
}

class XplainAdapter extends BaseAdapter{
    private AssetManager assetManager;
    private LayoutInflater inflater;
    private ActionBarActivity actionBarActivity;
    private Object[] xplainVideos;
    private Context context;

    public XplainAdapter(AssetManager AM, LayoutInflater layoutInflater, Context context, ActionBarActivity activity){
        this.assetManager = AM;
        this.inflater = layoutInflater;
        this.context = context;
        this.actionBarActivity = activity;

        ArrayList<Model_Xplain> modelXplains = new ArrayList<Model_Xplain>();
        modelXplains.add(new Model_Xplain(0, "F4X DIS Card", "", "qXKpD49ID6k"));
        modelXplains.add(new Model_Xplain(1, "F4X Cabinet Introduction", "", "EOHUGaQLb8U"));
        modelXplains.add(new Model_Xplain(2, "Secondary PC Swap", "", "PESUqEY13YY"));
        modelXplains.add(new Model_Xplain(3, "F4X Module", "", "QD-5rzh56sM"));
        modelXplains.add(new Model_Xplain(4, "F4X Power Supply Change", "", "C0OOsPuJWB4"));
        modelXplains.add(new Model_Xplain(5, "Changing your camera", "", "CjYlce1IiSc"));
        modelXplains.add(new Model_Xplain(6, "EMC Temp Probe Swap", "", "gjVNf7RK114"));
        modelXplains.add(new Model_Xplain(7, "EMC Photocell Change", "", "CYoXvcyOroc"));
        modelXplains.add(new Model_Xplain(8, "Digital Billboard Control Box", "", "u5OCg2MNFI4"));
        modelXplains.add(new Model_Xplain(9, "Intelligent Safegaurds", "", "GMeSq-pU4Rw"));
        modelXplains.add(new Model_Xplain(10, "FTX Receiver Card", "", "C2iAMhhg6eQ"));
        modelXplains.add(new Model_Xplain(11, "DBB CPS 100 Overview", "", "jJTJD3YTsbs"));
        modelXplains.add(new Model_Xplain(12, "Braves discuss their F4X board", "", "oSIQcr8fZdM"));
        modelXplains.add(new Model_Xplain(13, "FTX Power Supply Change", "", "51SxPI0mDZs"));
        modelXplains.add(new Model_Xplain(14, "Secondary PC Swap", "", "yeW5I64V_dI"));
        modelXplains.add(new Model_Xplain(15, "FTX Module Rear Change", "", "rt4onYejPA8"));
        modelXplains.add(new Model_Xplain(16, "FTX Module Change", "", "LERJ-fumy90"));
        modelXplains.add(new Model_Xplain(17, "FTX Install", "", "UMcBXR1EwWU"));
        this.xplainVideos = modelXplains.toArray();

    }

    @Override
    public int getCount() {
        return this.xplainVideos.length + 1;
    }

    @Override
    public Object getItem(int position) {
        if (position == 0){
            return null;
        }else {
            return this.xplainVideos[position - 1];
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Model_Xplain xplain = (Model_Xplain) getItem(position);
        ImageView thumbnail;
        ProgressBar progressBar;
        TextView label;

        switch (position){
            case 0:
                convertView = this.inflater.inflate(R.layout.cell_xplain_header_phone, parent, false);

                TextView xplainHeaderLabel = (TextView) convertView.findViewById(R.id.xplainHeader);
                ModelManager.convertLabelToHNThin(this.assetManager, xplainHeaderLabel);
                break;

            case 1:
                convertView = this.inflater.inflate(R.layout.cell_xplain_showcase_phone, parent, false);
                thumbnail = (ImageView) convertView.findViewById(R.id.xplainThumbnail);
                progressBar = (ProgressBar) convertView.findViewById(R.id.xplainProgressBar);
                ModelManager.populateImageView(
                        this.actionBarActivity,
                        String.format("http://f4xdigitalbillboards.com/app/xd/xplain_images/%s.png", position),
                        thumbnail,
                        progressBar
                );
                label = (TextView) convertView.findViewById(R.id.xplainTitle);
                label.setText(xplain.youtubeTitle);
                ImageButton playBtn = (ImageButton) convertView.findViewById(R.id.xplainPlayBtn);
                playBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ModalWebView.url = String.format("https://www.youtube.com/embed/%s?autoplay=1&vq=hd1080", xplain.youtubeID);
                        actionBarActivity.startActivity(
                                new android.content.Intent(
                                        actionBarActivity.getApplicationContext(),
                                        ModalWebView.class
                                )
                        );
                    }
                });

                break;

            default:
                convertView = this.inflater.inflate(R.layout.cell_xplain_phone, parent, false);
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ModalWebView.url = String.format("https://www.youtube.com/embed/%s?autoplay=1&vq=hd1080", xplain.youtubeID);
                        actionBarActivity.startActivity(
                                new android.content.Intent(
                                        actionBarActivity.getApplicationContext(),
                                        ModalWebView.class
                                )
                        );
                    }
                });

                thumbnail = (ImageView) convertView.findViewById(R.id.xplainThumbnail);
                progressBar = (ProgressBar) convertView.findViewById(R.id.xplainProgressBar);
                ModelManager.populateImageView(
                        this.actionBarActivity,
                        String.format("http://f4xdigitalbillboards.com/app/xd/xplain_images/%s.png", position),
                        thumbnail,
                        progressBar
                );
                label = (TextView) convertView.findViewById(R.id.xplainTitle);
                label.setText(xplain.youtubeTitle);

                break;
        }

        return convertView;
    }
}

class XplainTabletAdapter extends BaseAdapter {

    private AssetManager assetManager;
    private LayoutInflater inflater;
    private ActionBarActivity actionBarActivity;
    private Object[] xplainVideos;
    private Context context;

    public XplainTabletAdapter(AssetManager AM, LayoutInflater layoutInflater, Context context, ActionBarActivity activity) {
        this.assetManager = AM;
        this.inflater = layoutInflater;
        this.context = context;
        this.actionBarActivity = activity;

        ArrayList<Model_Xplain> modelXplains = new ArrayList<Model_Xplain>();
        modelXplains.add(new Model_Xplain(0, "F4X DIS Card", "", "qXKpD49ID6k"));
        modelXplains.add(new Model_Xplain(1, "F4X Cabinet Introduction", "", "EOHUGaQLb8U"));
        modelXplains.add(new Model_Xplain(2, "Secondary PC Swap", "", "PESUqEY13YY"));
        modelXplains.add(new Model_Xplain(3, "F4X Module", "One of our Xperts explains the simple change out.", "QD-5rzh56sM"));
        modelXplains.add(new Model_Xplain(4, "F4X Power Supply Change", "The simple replacement of the F4X power supply can be performed in under a minute.", "C0OOsPuJWB4"));
        modelXplains.add(new Model_Xplain(5, "Changing your camera", "", "CjYlce1IiSc"));
        modelXplains.add(new Model_Xplain(6, "EMC Temp Probe Swap", "", "gjVNf7RK114"));
        modelXplains.add(new Model_Xplain(7, "EMC Photocell Change", "", "CYoXvcyOroc"));
        modelXplains.add(new Model_Xplain(8, "Digital Billboard Control Box", "", "u5OCg2MNFI4"));
        modelXplains.add(new Model_Xplain(9, "Intelligent Safegaurds", "Our digital billboards deliver the ultimate peace of mind.", "GMeSq-pU4Rw"));
        modelXplains.add(new Model_Xplain(10, "FTX Receiver Card", "The simple replacement of the F4X power supply can be performed in under a minute.", "C2iAMhhg6eQ"));
        modelXplains.add(new Model_Xplain(11, "DBB CPS 100 Overview", "", "jJTJD3YTsbs"));
        modelXplains.add(new Model_Xplain(12, "Braves discuss their F4X board", "", "oSIQcr8fZdM"));
        modelXplains.add(new Model_Xplain(13, "FTX Power Supply Change", "", "51SxPI0mDZs"));
        modelXplains.add(new Model_Xplain(14, "Secondary PC Swap", "", "yeW5I64V_dI"));
        modelXplains.add(new Model_Xplain(15, "FTX Module Rear Change", "Simple module change from the rear.", "rt4onYejPA8"));
        modelXplains.add(new Model_Xplain(16, "FTX Module Change", "Watch the simple swap of an FTX module.", "LERJ-fumy90"));
        modelXplains.add(new Model_Xplain(17, "FTX Install", "", "UMcBXR1EwWU"));
        this.xplainVideos = modelXplains.toArray();
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return xplainVideos[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.cell_xplain, parent, false);
        int page = position;
        final int start = page * 6; // 0 * 6 = 0 start

        ImageView xplainBlockOneImageButton = (ImageView) convertView.findViewById(R.id.xplainBlock1);
        xplainBlockOneImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModalWebView.url = String.format("http://youtube.com/embed/%s?autoplay=1&vq=hd1080", ((Model_Xplain) xplainVideos[start]).youtubeID);
                actionBarActivity.startActivity(
                        new android.content.Intent(
                                actionBarActivity.getApplicationContext(),
                                ModalWebView.class
                        )
                );
            }
        });
        ProgressBar xplainBlockOneProgressBar = (ProgressBar) convertView.findViewById(R.id.xplainBlock1ProgressBar);
        ModelManager.populateImageView(this.actionBarActivity, String.format("http://f4xdigitalbillboards.com/app/xd/xplain_images/%s.png", ((Model_Xplain)this.xplainVideos[start]).ID), xplainBlockOneImageButton, xplainBlockOneProgressBar);
        TextView xplainBlockOneLabel = (TextView) convertView.findViewById(R.id.xplainBlock1Label);
        xplainBlockOneLabel.setText(((Model_Xplain) this.xplainVideos[start]).youtubeTitle);

        ImageView xplainBlockTwoImageButton = (ImageView) convertView.findViewById(R.id.xplainBlock2);
        xplainBlockTwoImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModalWebView.url = String.format("http://youtube.com/embed/%s?autoplay=1&vq=hd1080", ((Model_Xplain)xplainVideos[start+1]).youtubeID);
                actionBarActivity.startActivity(
                        new android.content.Intent(
                                actionBarActivity.getApplicationContext(),
                                ModalWebView.class
                        )
                );
            }
        });
        ProgressBar xplainBlockTwoProgressBar = (ProgressBar) convertView.findViewById(R.id.xplainBlock2ProgressBar);
        ModelManager.populateImageView(this.actionBarActivity, String.format("http://f4xdigitalbillboards.com/app/xd/xplain_images/%s.png", ((Model_Xplain) this.xplainVideos[start + 1]).ID), xplainBlockTwoImageButton, xplainBlockTwoProgressBar);
        TextView xplainBlockTwoLabel = (TextView) convertView.findViewById(R.id.xplainBlock2Label);
        xplainBlockTwoLabel.setText(((Model_Xplain) this.xplainVideos[start + 1]).youtubeTitle);

        ImageView xplainBlockThreeImageButton = (ImageView) convertView.findViewById(R.id.xplainBlock3);
        xplainBlockThreeImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModalWebView.url = String.format("http://youtube.com/embed/%s?autoplay=1&vq=hd1080", ((Model_Xplain)xplainVideos[start+2]).youtubeID);
                actionBarActivity.startActivity(
                        new android.content.Intent(
                                actionBarActivity.getApplicationContext(),
                                ModalWebView.class
                        )
                );
            }
        });
        ProgressBar xplainBlockThreeProgressBar = (ProgressBar) convertView.findViewById(R.id.xplainBlock3ProgressBar);
        ModelManager.populateImageView(this.actionBarActivity, String.format("http://f4xdigitalbillboards.com/app/xd/xplain_images/%s.png", ((Model_Xplain)this.xplainVideos[start+2]).ID), xplainBlockThreeImageButton, xplainBlockThreeProgressBar);
        TextView xplainBlockThreeLabel = (TextView) convertView.findViewById(R.id.xplainBlock3Label);
        xplainBlockThreeLabel.setText(((Model_Xplain)this.xplainVideos[start+2]).youtubeTitle);

        ImageView xplainBlockFourImageButton = (ImageView) convertView.findViewById(R.id.xplainBlock4);
        xplainBlockFourImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModalWebView.url = String.format("http://youtube.com/embed/%s?autoplay=1&vq=hd1080", ((Model_Xplain)xplainVideos[start+3]).youtubeID);
                actionBarActivity.startActivity(
                        new android.content.Intent(
                                actionBarActivity.getApplicationContext(),
                                ModalWebView.class
                        )
                );
            }
        });
        ProgressBar xplainBlockFourProgressBar = (ProgressBar) convertView.findViewById(R.id.xplainBlock4ProgressBar);
        ModelManager.populateImageView(this.actionBarActivity, String.format("http://f4xdigitalbillboards.com/app/xd/xplain_images/%s.png", ((Model_Xplain)this.xplainVideos[start+3]).ID), xplainBlockFourImageButton, xplainBlockFourProgressBar);
        TextView xplainBlockFourLabel = (TextView) convertView.findViewById(R.id.xplainBlock4Label);
        TextView xplainBlockFourDescriptionLabel = (TextView) convertView.findViewById(R.id.xplainBlock4DescriptionLabel);
        xplainBlockFourLabel.setText(((Model_Xplain)this.xplainVideos[start+3]).youtubeTitle);
        xplainBlockFourDescriptionLabel.setText(((Model_Xplain)this.xplainVideos[start+3]).youtubeDescription);

        ImageView xplainBlockFiveImageButton = (ImageView) convertView.findViewById(R.id.xplainBlock5);
        xplainBlockFiveImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModalWebView.url = String.format("http://youtube.com/embed/%s?autoplay=1&vq=hd1080", ((Model_Xplain)xplainVideos[start+4]).youtubeID);
                actionBarActivity.startActivity(
                        new android.content.Intent(
                                actionBarActivity.getApplicationContext(),
                                ModalWebView.class
                        )
                );
            }
        });
        ProgressBar xplainBlockFiveProgressBar = (ProgressBar) convertView.findViewById(R.id.xplainBlock5ProgressBar);
        ModelManager.populateImageView(this.actionBarActivity, String.format("http://f4xdigitalbillboards.com/app/xd/xplain_images/%s.png", ((Model_Xplain)this.xplainVideos[start+4]).ID), xplainBlockFiveImageButton, xplainBlockFiveProgressBar);
        TextView xplainBlockFiveLabel = (TextView) convertView.findViewById(R.id.xplainBlock5Label);
        TextView xplainBlockFiveDescriptionLabel = (TextView) convertView.findViewById(R.id.xplainBlock5DescriptionLabel);
        xplainBlockFiveLabel.setText(((Model_Xplain)this.xplainVideos[start+4]).youtubeTitle);
        xplainBlockFiveDescriptionLabel.setText(((Model_Xplain)this.xplainVideos[start+4]).youtubeDescription);

        ImageView xplainBlockSixImageButton = (ImageView) convertView.findViewById(R.id.xplainBlock6);
        xplainBlockSixImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModalWebView.url = String.format("http://youtube.com/embed/%s?autoplay=1&vq=hd1080", ((Model_Xplain) xplainVideos[start+5]).youtubeID);
                actionBarActivity.startActivity(
                        new android.content.Intent(
                                actionBarActivity.getApplicationContext(),
                                ModalWebView.class
                        )
                );
            }
        });
        TextView xplainBlockSixLabel = (TextView) convertView.findViewById(R.id.xplainBlock6Label);
        ProgressBar xplainBlockSixProgressBar = (ProgressBar) convertView.findViewById(R.id.xplainBlock6ProgressBar);
        xplainBlockSixLabel.setText(((Model_Xplain)this.xplainVideos[start+5]).youtubeTitle);
        ModelManager.populateImageView(this.actionBarActivity, String.format("http://f4xdigitalbillboards.com/app/xd/xplain_images/%s.png", ((Model_Xplain)this.xplainVideos[start+5]).ID), xplainBlockSixImageButton, xplainBlockSixProgressBar);

        return convertView;
    }
}