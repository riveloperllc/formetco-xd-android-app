package com.formetco.formetcoxd;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import java.util.ArrayList;

/**
 * Created by austinsweat on 7/9/15.
 */
public class Xpert extends ActionBarActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ModelManager.isPhone(this)) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //Force Landscape mode for all devices.
        }
        setContentView(R.layout.xpert_layout);
        if (ModelManager.isPhone(this)) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //Force Landscape mode for all devices.
        }
        ModelManager.activitySetup(this, false);

        if (!ModelManager.isPhone(this)){
            RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.xpertView);
            /*mainLayout.setBackgroundDrawable(
                    new BitmapDrawable(
                            ModelManager.decodeSampledBitmapFromResource(
                                    this.getResources(),
                                    R.drawable.background_tablet_xpert,
                                    mainLayout.getWidth(),
                                    mainLayout.getHeight()
                            )
                    )
            );*/
            try {
                TextView labelXpert = (TextView) findViewById(R.id.xpertLabel);
                ModelManager.convertLabelToHNThin(this.getAssets(), labelXpert);

                TextView descriptionXpert = (TextView) findViewById(R.id.xpertDescriptionLabel);
                //ModelManager.convertLabelToHNLight(this.getAssets(), descriptionXpert);

                ActionBarActivity convertView = this;
                final ActionBarActivity actionBarActivity = this;
                TextView nameFieldLabel = (TextView) convertView.findViewById(R.id.xpertFieldLabelName);
                final EditText nameField = (EditText) convertView.findViewById(R.id.xpertFieldName);
                nameField.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        ModelManager.xpert_Name = nameField.getText().toString();
                        return true;
                    }
                });
                TextView orgFieldLabel = (TextView) convertView.findViewById(R.id.xpertFieldLabelOrg);
                final EditText orgField = (EditText) convertView.findViewById(R.id.xpertFieldOrg);
                orgField.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        ModelManager.xpert_Org = orgField.getText().toString();
                        return true;
                    }
                });
                TextView phoneFieldLabel = (TextView) convertView.findViewById(R.id.xpertFieldLabelPhone);
                final EditText phoneField = (EditText) convertView.findViewById(R.id.xpertFieldPhone);
                phoneField.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        ModelManager.xpert_Phone = phoneField.getText().toString();
                        return true;
                    }
                });
                TextView emailFieldLabel = (TextView) convertView.findViewById(R.id.xpertFieldLabelEmail);
                final EditText emailField = (EditText) convertView.findViewById(R.id.xpertFieldEmail);
                emailField.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        ModelManager.xpert_Email = emailField.getText().toString();
                        return true;
                    }
                });
                TextView questionsFieldLabel = (TextView) convertView.findViewById(R.id.xpertFieldLabelQuestions);
                final EditText questionsField = (EditText) convertView.findViewById(R.id.xpertFieldQuestions);
                questionsField.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        ModelManager.xpert_Questions = questionsField.getText().toString();
                        return true;
                    }
                });

                Button submitBtn = (Button) convertView.findViewById(R.id.xpertFieldSubmitBtn);
                submitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ModelManager.xpert_Name.isEmpty() || ModelManager.xpert_Org.isEmpty() || ModelManager.xpert_Email.isEmpty() || ModelManager.xpert_Phone.isEmpty() || ModelManager.xpert_Questions.isEmpty()) {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                            builder1.setTitle("Whoops!");
                            builder1.setMessage("Please check the form above, and try again!");
                            builder1.setCancelable(true);
                            builder1.setPositiveButton("Okay",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } else {
                            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
                            asyncHttpClient.get(String.format("http://f4xdigitalbillboards.com/app/api.php?_app=xd&cmd=xpert&name=%s&org=%s&phone=%s&email=%s&questions=%s", ModelManager.xpert_Name, ModelManager.xpert_Org, ModelManager.xpert_Phone, ModelManager.xpert_Email, ModelManager.xpert_Questions), new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                                    builder1.setTitle("Message sent SuXessfully");
                                    builder1.setMessage("One of our Xpert team members will be in touch soon to confirm your appointment. You can reach us immediately at (800) 367 6382.");
                                    builder1.setCancelable(true);
                                    builder1.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                                    AlertDialog alert11 = builder1.create();
                                    alert11.show();
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                                    builder1.setTitle("Error!");
                                    builder1.setMessage("Please check your internet connection, and try again!");
                                    builder1.setCancelable(true);
                                    builder1.setPositiveButton("Okay",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });

                                    AlertDialog alert11 = builder1.create();
                                    alert11.show();
                                }
                            });
                        }
                    }
                });
            }catch(Exception e){

            }
        }else{
            ListView xpertListView = (ListView) findViewById(R.id.xpertListView);
            xpertListView.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            xpertListView.setDivider(new ColorDrawable(Color.argb(0, 0, 0, 0)));
            xpertListView.setAdapter(new XpertAdapter(this, this.getLayoutInflater(), this.getAssets()));
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }
}

class XpertAdapter extends BaseAdapter{

    private Object[] views;
    private LayoutInflater inflater;
    private AssetManager assetManager;
    private ActionBarActivity actionBarActivity;

    public XpertAdapter(ActionBarActivity actionBarActivity, LayoutInflater inflater, AssetManager AM){
        ArrayList<Integer> tmp_views = new ArrayList<Integer>();
        tmp_views.add(R.layout.cell_xpert_header_phone);
        tmp_views.add(R.layout.cell_xpert_fields_phone);
        this.views = tmp_views.toArray();
        this.inflater = inflater;
        this.assetManager = AM;
        this.actionBarActivity = actionBarActivity;
    }

    @Override
    public int getCount() {
        return views.length;
    }

    @Override
    public Object getItem(int position) {
        return views[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Integer _tmpView = (Integer) views[position];
        convertView = this.inflater.inflate(_tmpView, parent, false);

        switch (position){
            case 0:
                TextView headerLabel = (TextView) convertView.findViewById(R.id.xpertLabel);
                TextView descriptionLabel = (TextView) convertView.findViewById(R.id.xpertDescriptionLabel);
                ModelManager.convertLabelToHNThin(this.assetManager, headerLabel);
               // ModelManager.convertLabelToHNLight(this.assetManager, descriptionLabel);

                Button callBtn = (Button) convertView.findViewById(R.id.xpertFieldCallBtn);
                callBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(actionBarActivity);
                        alertDialog.setTitle("Contact Us");
                        alertDialog.setMessage("Dial +1 (800) 204-4386?");
                        alertDialog.setCancelable(true);
                        alertDialog.setPositiveButton("Call",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(Intent.ACTION_DIAL);
                                        intent.setData(Uri.parse("tel:18002044386"));
                                        actionBarActivity.startActivity(intent);
                                    }
                                });

                        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        AlertDialog alertDialog1 = alertDialog.create();
                        alertDialog1.show();
                    }
                });

                break;

            case 1:
                TextView nameFieldLabel = (TextView) convertView.findViewById(R.id.xpertFieldLabelName);
                final EditText nameField = (EditText) convertView.findViewById(R.id.xpertFieldName);
                nameField.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        ModelManager.xpert_Name = nameField.getText().toString();
                        return true;
                    }
                });
                TextView orgFieldLabel = (TextView) convertView.findViewById(R.id.xpertFieldLabelOrg);
                final EditText orgField = (EditText) convertView.findViewById(R.id.xpertFieldOrg);
                orgField.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        ModelManager.xpert_Org = orgField.getText().toString();
                        return true;
                    }
                });
                TextView phoneFieldLabel = (TextView) convertView.findViewById(R.id.xpertFieldLabelPhone);
                final EditText phoneField = (EditText) convertView.findViewById(R.id.xpertFieldPhone);
                phoneField.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        ModelManager.xpert_Phone = phoneField.getText().toString();
                        return true;
                    }
                });
                TextView emailFieldLabel = (TextView) convertView.findViewById(R.id.xpertFieldLabelEmail);
                final EditText emailField = (EditText) convertView.findViewById(R.id.xpertFieldEmail);
                emailField.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        ModelManager.xpert_Email = emailField.getText().toString();
                        return true;
                    }
                });
                TextView questionsFieldLabel = (TextView) convertView.findViewById(R.id.xpertFieldLabelQuestions);
                final EditText questionsField = (EditText) convertView.findViewById(R.id.xpertFieldQuestions);
                questionsField.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        ModelManager.xpert_Questions = questionsField.getText().toString();
                        return true;
                    }
                });

                Button submitBtn = (Button) convertView.findViewById(R.id.xpertFieldSubmitBtn);
                submitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ModelManager.xpert_Name.isEmpty() || ModelManager.xpert_Org.isEmpty() || ModelManager.xpert_Email.isEmpty() || ModelManager.xpert_Phone.isEmpty() || ModelManager.xpert_Questions.isEmpty()){
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                            builder1.setTitle("Whoops!");
                            builder1.setMessage("Please check the form above, and try again!");
                            builder1.setCancelable(true);
                            builder1.setPositiveButton("Okay",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        }else {
                            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
                            asyncHttpClient.get(String.format("http://f4xdigitalbillboards.com/app/api.php?_app=xd&cmd=xpert&name=%s&org=%s&phone=%s&email=%s&questions=%s", ModelManager.xpert_Name, ModelManager.xpert_Org, ModelManager.xpert_Phone, ModelManager.xpert_Email, ModelManager.xpert_Questions), new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                                    builder1.setTitle("Message sent SuXessfully");
                                    builder1.setMessage("One of our Xpert team members will be in touch soon to confirm your appointment. You can reach us immediately at (800) 367 6382.");
                                    builder1.setCancelable(true);
                                    builder1.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                                    AlertDialog alert11 = builder1.create();
                                    alert11.show();
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                                    builder1.setTitle("Error!");
                                    builder1.setMessage("Please check your internet connection, and try again!");
                                    builder1.setCancelable(true);
                                    builder1.setPositiveButton("Okay",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });

                                    AlertDialog alert11 = builder1.create();
                                    alert11.show();
                                }
                            });
                        }
                    }
                });
                break;

            default:
                break;
        }

        return convertView;
    }
}