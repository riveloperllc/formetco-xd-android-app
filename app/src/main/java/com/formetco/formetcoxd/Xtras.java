package com.formetco.formetcoxd;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by austinsweat on 7/8/15.
 */
public class Xtras extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ModelManager.isNetworkAvailable(this)) {
            if (ModelManager.isPhone(this)) {
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //Force Landscape mode for all devices.
            }
            setContentView(R.layout.xtras_layout);
            if (ModelManager.isPhone(this)) {
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //Force Landscape mode for all devices.
            }
            ModelManager.activitySetup(this, false);
        }else{
            setContentView(R.layout.missing_layout);
            ModelManager.activitySetup(this, false);
            TextView missingHeader = (TextView) findViewById(R.id.missingHeader);
            ModelManager.convertLabelToHNThin(this.getAssets(), missingHeader);
            TextView missingDescription = (TextView) findViewById(R.id.missingDescription);
            return;
        }

        try {
            if (ModelManager.isPhone(this)) {
                //Create listview adapter
                ListView xtrasListView = (ListView) findViewById(R.id.xtraslistview);
                xtrasListView.setAdapter(new XtrasListViewAdapater(this, this.getAssets(), getApplicationContext()));
            } else {
                //Create other adapter
                TextView xtrasLabel = (TextView) findViewById(R.id.xtraslabel);
                xtrasLabel.setTextColor(Color.argb(255, 153, 154, 157));
                ModelManager.convertLabelToHNThin(this.getAssets(), xtrasLabel);

                GridView xtrasGridView = (GridView) findViewById(R.id.xtrasgridview);
                xtrasGridView.setAdapter(new XtrasListViewAdapater(this, this.getAssets(), getApplicationContext()));
            }
        }catch (Exception e){

        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }
}

class XtrasListViewAdapater extends BaseAdapter{
    private Object[] xtras;
    private LayoutInflater inflater;
    private Context context;
    private AssetManager assetManager;
    private ActionBarActivity actionBarActivity;

    public XtrasListViewAdapater(ActionBarActivity ABA, AssetManager AM, Context context){
        ArrayList<Model_Xtra> modelXtras = new ArrayList<Model_Xtra>();
        modelXtras.add(new Model_Xtra("Digital-Billboards-Deliver-Critical-Information", "Digital Billboards Deliver Critical Information", false));
        modelXtras.add(new Model_Xtra("Factors-affecting-electrical-costs", "Factors affecting electrical costs", false));
        modelXtras.add(new Model_Xtra("LED-Digital-Billboard-Color-Bit-Depth", "LED Digital Billboard Color Bit Depth", false));
        modelXtras.add(new Model_Xtra("The-Truth-about-Power-Specs", "The Truth about Power Specs", false));
        modelXtras.add(new Model_Xtra("Creative-Guideliness-for-LED-Digital-Billbaords", "Creative Guideliness for LED Digital Billbaords", true));
        modelXtras.add(new Model_Xtra("DBB-Data-Connections-for-your-Digital", "DBB Data Connections for your Digital", true));
        modelXtras.add(new Model_Xtra("DBB-Intelligent-Safe-Guards", "DBB Intelligent Safe Guards", false));
        modelXtras.add(new Model_Xtra("Ready-to-Advertise-Using-Digital", "Ready to Advertise Using Digital", true));
        modelXtras.add(new Model_Xtra("Formetcos-Control-Center", "Formetcos Control Center", true));
        this.xtras = modelXtras.toArray();
        this.context = context;
        this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.assetManager = AM;
        this.actionBarActivity = ABA;
    }

    @Override
    public int getCount() {
        if (ModelManager.isPhone(this.actionBarActivity)) {
            return xtras.length + 1;
        }else{
            return xtras.length;
        }
    }

    @Override
    public Object getItem(int position) {
        if (position == 0){
            return null;
        }else {
            return xtras[position];
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position == 0 && ModelManager.isPhone(this.actionBarActivity)){
            convertView = this.inflater.inflate(R.layout.cell_xtras_header, parent, false);
            TextView xtrasLabel = (TextView) convertView.findViewById(R.id.xtraslabel);
            xtrasLabel.setTextColor(Color.argb(255, 153, 154, 157));
            xtrasLabel.setTypeface(Typeface.createFromAsset(this.assetManager, "fonts/HN_Thin.ttf"));
        }else {
            Model_Xtra modelXtra;
            if (ModelManager.isPhone(this.actionBarActivity)) {
                modelXtra = (Model_Xtra) xtras[position - 1];
            }else{
                modelXtra = (Model_Xtra) xtras[position];
            }
            final Model_Xtra tmpXtra = modelXtra;
            convertView = this.inflater.inflate(R.layout.cell_xtra, parent, false);
            ImageView xtra_lock = (ImageView) convertView.findViewById(R.id.xtra_lock);

            if (!modelXtra.locked){
                xtra_lock.setVisibility(View.INVISIBLE);
            }

            final ProgressBar xtra_progressBar = (ProgressBar) convertView.findViewById(R.id.xtra_progress_bar);
            xtra_progressBar.setActivated(true);

            final ImageButton xtra_overlay = (ImageButton) convertView.findViewById(R.id.xtra_overlay);
            ModelManager.populateImageView(this.actionBarActivity, "http://www.formetco.com/app/xtras/thumbnails/" + modelXtra.fileName + ".jpg", xtra_overlay, xtra_progressBar);

            xtra_overlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tmpXtra.locked && !ModelManager.isLoggedIn()) {
                        actionBarActivity.startActivity(new android.content.Intent(actionBarActivity.getApplicationContext(), Login.class));
                    } else {
                        ModalWebView.url = String.format("https://docs.google.com/viewer?url=http://www.formetco.com/app/xtras/downloads/%s.pdf", tmpXtra.fileName);
                        actionBarActivity.startActivity(
                                new android.content.Intent(
                                        actionBarActivity.getApplicationContext(),
                                        ModalWebView.class
                                )
                        );
                    }
                }
            });

            TextView xtra_label = (TextView) convertView.findViewById(R.id.xtra_label);

            xtra_label.setText(modelXtra.properName);
        }
        return convertView;
    }
}
