package com.formetco.formetcoxd;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by austinsweat on 7/10/15.
 */
public class Xamine extends ActionBarActivity {
    private WebView webView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ModelManager.isPhone(this)) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //Force Landscape mode for all devices.
        }

        if (ModelManager.isNetworkAvailable(this)) {
            setContentView(R.layout.xamine_layout);
            ModelManager.activitySetup(this, false);
        }else{
            setContentView(R.layout.missing_layout);
            ModelManager.activitySetup(this, false);
            TextView missingHeader = (TextView) findViewById(R.id.missingHeader);
            ModelManager.convertLabelToHNThin(this.getAssets(), missingHeader);
            TextView missingDescription = (TextView) findViewById(R.id.missingDescription);
            return;
        }

        if (ModelManager.isPhone(this)) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //Force Landscape mode for all devices.
        }

        if (ModelManager.isNetworkAvailable(this)){
            //if (ModelManager.isPhone(this)){
                TextView xamineLabel = (TextView) findViewById(R.id.xaminelabel);
                ModelManager.convertLabelToHNThin(this.getAssets(), xamineLabel);
            //}

            if (ModelManager.isLoggedIn()){
                ImageButton cloudBtn = (ImageButton) findViewById(R.id.xamineCloudBtn);
                cloudBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        webView.goBack();
                    }
                });

                this.progressBar = (ProgressBar) findViewById(R.id.xamineProgressBar);

                final ActionBarActivity actionBarActivity = this;
                webView = (WebView) findViewById(R.id.xamineWebView);
                AsyncHttpClient httpClient = new AsyncHttpClient();
                httpClient.get("http://f4xdigitalbillboards.com/app/api.php?_app=xd&cmd=xamine&token=" + ModelManager.userToken, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        String response = new String(responseBody);
                        try {
                            JSONObject jObj = new JSONObject(response);
                            String link = jObj.getString("link");
                            webView.setWebViewClient(new WebViewController());
                            webView.getSettings().setJavaScriptEnabled(true);
                            webView.loadUrl(link);
                            progressBar.setVisibility(View.INVISIBLE);
                        } catch (JSONException e) {
                            /*Server Down, or JSON Problem!*/
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                            builder1.setMessage("You must have an internet connection to continue.");
                            builder1.setCancelable(true);
                            builder1.setPositiveButton("Okay",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.d("Failed", "We failed");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                        builder1.setMessage("You must have an internet connection to continue.");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Okay",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });

                ImageButton shareBtn = (ImageButton) findViewById(R.id.xamineShareBtn);
                shareBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //View view = findViewById(R.id.layout);//your layout id
                        webView.getRootView();
                        String state = Environment.getExternalStorageState();


                        if (Environment.MEDIA_MOUNTED.equals(state)) {
                            File picDir = new File(Environment.getExternalStorageDirectory() + "/myPic");
                            if (!picDir.exists()) {
                                picDir.mkdir();
                            }
                            webView.setDrawingCacheEnabled(true);
                            webView.buildDrawingCache(true);
                            Bitmap bitmap = webView.getDrawingCache();
//          Date date = new Date();
                            String fileName = "xamine" + ".jpg";
                            File picFile = new File(picDir + "/" + fileName);
                            try {
                                picFile.createNewFile();
                                FileOutputStream picOut = new FileOutputStream(picFile);
                                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), (int) (bitmap.getHeight() / 1.2));
                                boolean saved = bitmap.compress(Bitmap.CompressFormat.JPEG, 100, picOut);
                                if (saved) {
                                    Toast.makeText(getApplicationContext(), "Image saved to your device Pictures " + "directory!", Toast.LENGTH_SHORT).show();
                                } else {
                                    //Error
                                }
                                picOut.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            webView.destroyDrawingCache();
                            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                            sharingIntent.setType("image/jpeg");
                            sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(picFile.getAbsolutePath()));
                            startActivity(Intent.createChooser(sharingIntent, "Share via"));
                        } else {
                            //Error

                        }
                    }
                });

            }else{
                this.startActivity(new android.content.Intent(this.getApplicationContext(), Login.class));
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ModelManager.isNetworkAvailable(this)){
            //if (ModelManager.isPhone(this)){
            TextView xamineLabel = (TextView) findViewById(R.id.xaminelabel);
            ModelManager.convertLabelToHNThin(this.getAssets(), xamineLabel);
            //}

            if (ModelManager.isLoggedIn()){
                ImageButton cloudBtn = (ImageButton) findViewById(R.id.xamineCloudBtn);
                cloudBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        webView.goBack();
                    }
                });

                this.progressBar = (ProgressBar) findViewById(R.id.xamineProgressBar);

                final ActionBarActivity actionBarActivity = this;
                webView = (WebView) findViewById(R.id.xamineWebView);
                AsyncHttpClient httpClient = new AsyncHttpClient();
                httpClient.get("http://f4xdigitalbillboards.com/app/api.php?_app=xd&cmd=xamine&token=" + ModelManager.userToken, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        String response = new String(responseBody);
                        try {
                            JSONObject jObj = new JSONObject(response);
                            String link = jObj.getString("link");
                            webView.setWebViewClient(new WebViewController());
                            webView.getSettings().setJavaScriptEnabled(true);
                            webView.loadUrl(link);
                            progressBar.setVisibility(View.INVISIBLE);
                        } catch (Exception e) {
                            /*Server Down, or JSON Problem!*/
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                            builder1.setMessage("You must have an internet connection to continue.");
                            builder1.setCancelable(true);
                            builder1.setPositiveButton("Okay",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.d("Failed", "We failed");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                        builder1.setMessage("You must have an internet connection to continue.");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Okay",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });

                ImageButton shareBtn = (ImageButton) findViewById(R.id.xamineShareBtn);
                shareBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //View view = findViewById(R.id.layout);//your layout id
                        webView.getRootView();
                        String state = Environment.getExternalStorageState();


                        if (Environment.MEDIA_MOUNTED.equals(state)) {
                            File picDir = new File(Environment.getExternalStorageDirectory() + "/formetco");
                            if (!picDir.exists()) {
                                picDir.mkdir();
                            }
                            webView.setDrawingCacheEnabled(true);
                            webView.buildDrawingCache(true);
                            Bitmap bitmap = webView.getDrawingCache();
//          Date date = new Date();

                            String fileName = "xamine" + ".jpg";
                            File picFile = new File(picDir + "/" + fileName);
                            try {
                                picFile.createNewFile();
                                FileOutputStream picOut = new FileOutputStream(picFile);
                                addImageToGallery(picFile.getPath(), actionBarActivity);
                                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), (int) (bitmap.getHeight() / 1.2));
                                boolean saved = bitmap.compress(Bitmap.CompressFormat.JPEG, 100, picOut);
                                if (saved) {
                                    Toast.makeText(getApplicationContext(), "Image saved to your device Pictures " + "directory!", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Could not save image to your device.", Toast.LENGTH_LONG).show();
                                }
                                picOut.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            webView.destroyDrawingCache();
                            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                            sharingIntent.setType("image/jpeg");
                            sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(picFile.getAbsolutePath()));
                            startActivity(Intent.createChooser(sharingIntent, "Share via"));
                        } else {
                            //Error
                            Log.e("Error", "with image");
                        }
                    }
                });

            }else{
                //this.startActivity(new android.content.Intent(this.getApplicationContext(), Login.class));
            }
        }
    }

    public static void addImageToGallery(final String filePath, final Context context) {

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filePath);

        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            this.webView.destroy();
        }catch (Exception e){

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            this.webView.destroy();
        }catch (Exception e){

        }
    }
}
