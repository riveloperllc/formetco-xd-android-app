package com.formetco.formetcoxd;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.ProgressBar;

/**
 * Created by austinsweat on 7/10/15.
 */
public class Splash extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);

        if(!this.getResources().getBoolean(R.bool.portrait_only)){
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        ProgressBar splashLoadingBar = (ProgressBar) findViewById(R.id.splash_progressBar);
        final Context context = this.getApplicationContext();
        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized(this){
                        wait(3000);
                    }
                }
                catch(InterruptedException ex){ }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new android.content.Intent(context, Home.class));
                    }
                });
            }
        };

        thread.start();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        setContentView(R.layout.splash_layout);

        if(!this.getResources().getBoolean(R.bool.portrait_only)){
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        ProgressBar splashLoadingBar = (ProgressBar) findViewById(R.id.splash_progressBar);
        final Context context = this.getApplicationContext();
        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized(this){
                        wait(3000);
                    }
                }
                catch(InterruptedException ex){ }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new android.content.Intent(context, Home.class));
                    }
                });
            }
        };

        thread.start();
    }
}
