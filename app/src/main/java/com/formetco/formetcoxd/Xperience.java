package com.formetco.formetcoxd;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by austinsweat on 7/9/15.
 */
public class Xperience extends ActionBarActivity{
    int _year = Calendar.getInstance().get(Calendar.YEAR);
    int _month = Calendar.getInstance().get(Calendar.MONTH);
    int _dayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ModelManager.isPhone(this)) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //Force Landscape mode for all devices.
        }else{
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        setContentView(R.layout.xperience_layout);
        if (ModelManager.isPhone(this)) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //Force Landscape mode for all devices.
        }else{
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        ModelManager.activitySetup(this, false);

       try {
            if (!ModelManager.isPhone(this)) {
                ActionBarActivity convertView = this;

                TextView headerLabel = (TextView) this.findViewById(R.id.xperienceLabel);
                TextView descriptionLabel = (TextView) this.findViewById(R.id.xpertDescriptionLabel);
                ModelManager.convertLabelToHNThin(this.getAssets(), headerLabel);
                final Spinner eventTypeSpinner = (Spinner) this.findViewById(R.id.xperienceEventField);

                eventTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        TextView tv = (TextView) eventTypeSpinner.getSelectedView();
                        tv.setTypeface(Typeface.DEFAULT_BOLD); //to make text bold
                        ModelManager.xperience_EventType = tv.getText().toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                Button eventTypeTriggerBtn = (Button) this.findViewById(R.id.xperienceEventFieldTriggerBtn);
                eventTypeTriggerBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        eventTypeSpinner.performClick();
                    }
                });

                final EditText contactField = (EditText) convertView.findViewById(R.id.xperienceContactNameField);
                contactField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus){
                            ModelManager.xperience_ContactName = ((TextView)v).getText().toString();
                        }
                    }
                });
                final EditText orgField = (EditText) convertView.findViewById(R.id.xperienceOrgField);
                orgField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) {
                            ModelManager.xperience_Org = ((TextView) v).getText().toString();
                        }
                    }
                });
                final EditText emailField = (EditText) convertView.findViewById(R.id.xperienceEmailField);
                emailField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) {
                            ModelManager.xperience_Email = ((TextView) v).getText().toString();
                        }
                    }
                });
                final EditText phoneField = (EditText) convertView.findViewById(R.id.xperiencePhoneField);
                phoneField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) {
                            ModelManager.xperience_Phone = ((TextView) v).getText().toString();
                        }
                    }
                });

                final CalendarView cv = (CalendarView) this.findViewById(R.id.xperienceEventCalendarField);
                cv.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                        _year = year;
                        _month = month;
                        _dayOfMonth = dayOfMonth;

                        ModelManager.xperience_Day = _dayOfMonth;
                        ModelManager.xperience_Month = month;
                        ModelManager.xperience_Year = _year;
                    }
                });

                try {
                    Class<?> cvClass = cv.getClass();
                    Field field = cvClass.getDeclaredField("mMonthName");
                    field.setAccessible(true);

                    try {
                        TextView tv = (TextView) field.get(cv);
                        tv.setTextColor(Color.WHITE);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                    try {
                        ViewGroup vg = (ViewGroup) cv.getChildAt(0);
                        View child = vg.getChildAt(0);

                        if (child instanceof TextView) {
                            ((TextView) child).setTextColor(getResources().getColor(R.color.caldroid_white));
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }

                Button increaseDateBtn = (Button) this.findViewById(R.id.xperienceEventCalendarControlIncreaseBtn);
                increaseDateBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar c = Calendar.getInstance();
                        c.set(_year, _month, _dayOfMonth);
                        c.add(Calendar.MONTH, 1);

                        cv.setDate(c.getTimeInMillis(), false, true);
                    }
                });

                Button decreaseDateBtn = (Button) this.findViewById(R.id.xperienceEventCalendarControlDecreaseBtn);
                decreaseDateBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar c = Calendar.getInstance();
                        c.set(_year, _month, _dayOfMonth);
                        c.add(Calendar.MONTH, -1);

                        cv.setDate(c.getTimeInMillis(), false, true);
                    }
                });

                final ActionBarActivity actionBarActivity = this;

                final Spinner hourSpinner = (Spinner) convertView.findViewById(R.id.xperienceHour);

                Integer[] items = new Integer[] {1,2,3,4,5,6,7,8,9,10,11,12};
                ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(actionBarActivity.getApplicationContext(),android.R.layout.simple_spinner_item, items);
                hourSpinner.setAdapter(adapter);

                hourSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        TextView tv = (TextView) view;
                        tv.setTextColor(Color.BLACK);
                        ModelManager.xperience_Hour = position + 1;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                Button hourBtn = (Button) convertView.findViewById(R.id.xperienceHourBtn);
                hourBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hourSpinner.performClick();
                    }
                });

                final Spinner minuteSpinner = (Spinner) convertView.findViewById(R.id.xperienceMinute);
                Integer[] items2 = new Integer[] {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59};
                ArrayAdapter<Integer> adapter2 = new ArrayAdapter<Integer>(actionBarActivity.getApplicationContext(),android.R.layout.simple_spinner_item, items2);
                minuteSpinner.setAdapter(adapter2);
                minuteSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        TextView tv = (TextView) view;
                        tv.setTextColor(Color.BLACK);
                        ModelManager.xperience_Minute = position + 1;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                Button minuteBtn = (Button) convertView.findViewById(R.id.xperienceMinuteBtn);
                minuteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        minuteSpinner.performClick();
                    }
                });

                final Spinner timeOfDaySpinner = (Spinner) convertView.findViewById(R.id.xperienceTimeOfDay);
                timeOfDaySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        ModelManager.xperience_TimeOfDay = position + 1;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                Button timeOfDayBtn = (Button) convertView.findViewById(R.id.xperienceTimeOfDayBtn);
                timeOfDayBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        timeOfDaySpinner.performClick();
                    }
                });


                Button submitBtn = (Button) convertView.findViewById(R.id.xperienceSubmitBtn);
                submitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ModelManager.xperience_ContactName.isEmpty() || ModelManager.xperience_Org.isEmpty() || ModelManager.xperience_Email.isEmpty() || ModelManager.xperience_Phone.isEmpty()) {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity);
                            builder1.setTitle("Whoops!");
                            builder1.setMessage("Please check the form above, and try again!");
                            builder1.setCancelable(true);
                            builder1.setPositiveButton("Okay",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } else {
                            if (
                                    ((Calendar.getInstance()).get(Calendar.MONTH) > ModelManager.xperience_Year) ||
                                            ((Calendar.getInstance()).get(Calendar.MONTH) > ModelManager.xperience_Month && (Calendar.getInstance()).get(Calendar.YEAR) >= ModelManager.xperience_Year) ||
                                            ((Calendar.getInstance()).get(Calendar.DAY_OF_MONTH) > ModelManager.xperience_Day && (Calendar.getInstance()).get(Calendar.MONTH) >= ModelManager.xperience_Month)

                                    ) {
                                AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity);
                                builder1.setTitle("Sorry");
                                builder1.setMessage("Our Xperts have yet to perfect the Flux Capacitor. Until then please select a date after (today's date).");
                                builder1.setCancelable(true);
                                builder1.setPositiveButton("Okay",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });

                                AlertDialog alert11 = builder1.create();
                                alert11.show();

                            } else {
                                AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
                                asyncHttpClient.get(String.format("http://f4xdigitalbillboards.com/app/api.php?_app=xd&cmd=xperience&eventType=%s&name=%s&org=%s&email=%s&phone=%s&month=%s&day=%s&year=%s&hour=%s&minute=%s&timeofday=%s", ModelManager.xperience_EventType, ModelManager.xperience_ContactName, ModelManager.xperience_Org, ModelManager.xperience_Email, ModelManager.xperience_Phone, ModelManager.xperience_Month, ModelManager.xperience_Day, ModelManager.xperience_Year, ModelManager.xperience_Hour, ModelManager.xperience_Minute, ModelManager.xperience_TimeOfDay), new AsyncHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity);
                                        builder1.setTitle("Message sent SuXessfully");
                                        builder1.setMessage("One of our Xpert team members will be in touch soon to confirm your appointment. You can reach us immediately at (800) 367 6382.");
                                        builder1.setCancelable(true);
                                        builder1.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity);
                                        builder1.setTitle("Error!");
                                        builder1.setMessage("Please check your internet connection, and try again!");
                                        builder1.setCancelable(true);
                                        builder1.setPositiveButton("Okay",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });

                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();
                                    }
                                });
                            }
                        }
                    }
                });

            } else {
                ListView xperienceListView = (ListView) findViewById(R.id.xperienceListView);
                xperienceListView.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
                xperienceListView.setDivider(new ColorDrawable(Color.argb(0, 0, 0, 0)));
                xperienceListView.setAdapter(new XperienceAdapter(this, this.getLayoutInflater(), this.getAssets()));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }
}

class XperienceAdapter extends BaseAdapter{

    private Object[] views;
    private LayoutInflater inflater;
    private AssetManager assetManager;
    private ActionBarActivity actionBarActivity;

    public XperienceAdapter(ActionBarActivity actionBarActivity, LayoutInflater inflater, AssetManager AM){
        ArrayList<Integer> tmp_views = new ArrayList<Integer>();
        tmp_views.add(R.layout.cell_xperience_header_phone);
        tmp_views.add(R.layout.cell_xperience_column_one_phone);
        tmp_views.add(R.layout.cell_xperience_column_two_phone);
        this.views = tmp_views.toArray();
        this.inflater = inflater;
        this.assetManager = AM;
        this.actionBarActivity = actionBarActivity;
    }

    @Override
    public int getCount() {
        return views.length;
    }

    @Override
    public Object getItem(int position) {
        return views[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    int _year = Calendar.getInstance().get(Calendar.YEAR);
    int _month = Calendar.getInstance().get(Calendar.MONTH);
    int _dayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Integer _tmpView = (Integer) views[position];
        convertView = this.inflater.inflate(_tmpView, parent, false);

        switch (position){
            case 0:
                TextView headerLabel = (TextView) convertView.findViewById(R.id.xperienceLabel);
                TextView descriptionLabel = (TextView) convertView.findViewById(R.id.xpertDescriptionLabel);
                ModelManager.convertLabelToHNThin(this.assetManager, headerLabel);
                break;

            case 1:
                final Spinner eventTypeSpinner = (Spinner) convertView.findViewById(R.id.xperienceEventField);

                eventTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        TextView tv = (TextView) eventTypeSpinner.getSelectedView();
                        tv.setTypeface(Typeface.DEFAULT_BOLD); //to make text bold
                        ModelManager.xperience_EventType = tv.getText().toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                Button eventTypeTriggerBtn = (Button) convertView.findViewById(R.id.xperienceEventFieldTriggerBtn);
                eventTypeTriggerBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        eventTypeSpinner.performClick();
                    }
                });

                final EditText contactField = (EditText) convertView.findViewById(R.id.xperienceContactNameField);
                contactField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) {
                            ModelManager.xperience_ContactName = ((TextView) v).getText().toString();
                        }
                    }
                });
                final EditText orgField = (EditText) convertView.findViewById(R.id.xperienceOrgField);
                orgField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) {
                            ModelManager.xperience_Org = ((TextView) v).getText().toString();
                        }
                    }
                });
                final EditText emailField = (EditText) convertView.findViewById(R.id.xperienceEmailField);
                emailField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) {
                            ModelManager.xperience_Email = ((TextView) v).getText().toString();
                        }
                    }
                });
                final EditText phoneField = (EditText) convertView.findViewById(R.id.xperiencePhoneField);
                phoneField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) {
                            ModelManager.xperience_Phone = ((TextView) v).getText().toString();
                        }
                    }
                });
                break;

            case 2:
                final CalendarView cv = (CalendarView) convertView.findViewById(R.id.xperienceEventCalendarField);

                cv.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                        _year = year;
                        _month = month;
                        _dayOfMonth = dayOfMonth;

                        ModelManager.xperience_Day = _dayOfMonth;
                        ModelManager.xperience_Month = month;
                        ModelManager.xperience_Year = _year;
                    }
                });

                try
                {

                    Class<?> cvClass = cv.getClass();
                    Field field = cvClass.getDeclaredField("mMonthName");
                    field.setAccessible(true);

                    try
                    {
                        TextView tv = (TextView) field.get(cv);
                        tv.setTextColor(Color.WHITE);
                    }
                    catch (IllegalArgumentException e)
                    {
                        e.printStackTrace();
                    }
                    catch (IllegalAccessException e)
                    {
                        e.printStackTrace();
                    }
                }
                catch (NoSuchFieldException e)
                {
                    e.printStackTrace();
                }

                Button increaseDateBtn = (Button) convertView.findViewById(R.id.xperienceEventCalendarControlIncreaseBtn);
                increaseDateBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar c = Calendar.getInstance();
                        c.set(_year, _month, _dayOfMonth);
                        c.add(Calendar.MONTH, 1);

                        cv.setDate(c.getTimeInMillis(), false, true);
                    }
                });

                Button decreaseDateBtn = (Button) convertView.findViewById(R.id.xperienceEventCalendarControlDecreaseBtn);
                decreaseDateBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar c = Calendar.getInstance();
                        c.set(_year, _month, _dayOfMonth);
                        c.add(Calendar.MONTH, -1);

                        cv.setDate(c.getTimeInMillis(), false, true);
                    }
                });

                final Spinner hourSpinner = (Spinner) convertView.findViewById(R.id.xperienceHour);

                Integer[] items = new Integer[] {1,2,3,4,5,6,7,8,9,10,11,12};
                ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(actionBarActivity.getApplicationContext(),android.R.layout.simple_spinner_item, items);
                hourSpinner.setAdapter(adapter);

                hourSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        TextView tv = (TextView) view;
                        tv.setTextColor(Color.BLACK);
                        ModelManager.xperience_Hour = position + 1;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                Button hourBtn = (Button) convertView.findViewById(R.id.xperienceHourBtn);
                hourBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hourSpinner.performClick();
                    }
                });

                final Spinner minuteSpinner = (Spinner) convertView.findViewById(R.id.xperienceMinute);
                Integer[] items2 = new Integer[] {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59};
                ArrayAdapter<Integer> adapter2 = new ArrayAdapter<Integer>(actionBarActivity.getApplicationContext(),android.R.layout.simple_spinner_item, items2);
                minuteSpinner.setAdapter(adapter2);
                minuteSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        TextView tv = (TextView) view;
                        tv.setTextColor(Color.BLACK);
                        ModelManager.xperience_Minute = position + 1;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                Button minuteBtn = (Button) convertView.findViewById(R.id.xperienceMinuteBtn);
                minuteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        minuteSpinner.performClick();
                    }
                });

                final Spinner timeOfDaySpinner = (Spinner) convertView.findViewById(R.id.xperienceTimeOfDay);
                timeOfDaySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        ModelManager.xperience_TimeOfDay = position + 1;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                Button timeOfDayBtn = (Button) convertView.findViewById(R.id.xperienceTimeOfDayBtn);
                timeOfDayBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        timeOfDaySpinner.performClick();
                    }
                });

                Button submitBtn = (Button) convertView.findViewById(R.id.xperienceSubmitBtn);
                submitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ModelManager.xperience_ContactName.isEmpty() || ModelManager.xperience_Org.isEmpty() || ModelManager.xperience_Email.isEmpty() || ModelManager.xperience_Phone.isEmpty()) {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                            builder1.setTitle("Whoops!");
                            builder1.setMessage("Please check the form above, and try again!");
                            builder1.setCancelable(true);
                            builder1.setPositiveButton("Okay",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } else {
                            if (
                                    ((Calendar.getInstance()).get(Calendar.MONTH) > ModelManager.xperience_Year) ||
                                            ((Calendar.getInstance()).get(Calendar.MONTH) > ModelManager.xperience_Month && (Calendar.getInstance()).get(Calendar.YEAR) >= ModelManager.xperience_Year) ||
                                            ((Calendar.getInstance()).get(Calendar.DAY_OF_MONTH) > ModelManager.xperience_Day && (Calendar.getInstance()).get(Calendar.MONTH) >= ModelManager.xperience_Month)

                                    ) {
                                AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                                builder1.setTitle("Sorry");
                                builder1.setMessage("Our Xperts have yet to perfect the Flux Capacitor. Until then please select a date after (today's date).");
                                builder1.setCancelable(true);
                                builder1.setPositiveButton("Okay",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });

                                AlertDialog alert11 = builder1.create();
                                alert11.show();

                            } else {
                                AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
                                asyncHttpClient.get(String.format("http://f4xdigitalbillboards.com/app/api.php?_app=xd&cmd=xperience&eventType=%s&name=%s&org=%s&email=%s&phone=%s&month=%s&day=%s&year=%s&hour=%s&minute=%s&timeofday=%s", ModelManager.xperience_EventType, ModelManager.xperience_ContactName, ModelManager.xperience_Org, ModelManager.xperience_Email, ModelManager.xperience_Phone, ModelManager.xperience_Month, ModelManager.xperience_Day, ModelManager.xperience_Year, ModelManager.xperience_Hour, ModelManager.xperience_Minute, ModelManager.xperience_TimeOfDay), new AsyncHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                                        builder1.setTitle("Message sent SuXessfully");
                                        builder1.setMessage("One of our Xpert team members will be in touch soon to confirm your appointment. You can reach us immediately at (800) 367 6382.");
                                        builder1.setCancelable(true);
                                        builder1.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                                        builder1.setTitle("Error!");
                                        builder1.setMessage("Please check your internet connection, and try again!");
                                        builder1.setCancelable(true);
                                        builder1.setPositiveButton("Okay",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });

                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();
                                    }
                                });
                            }
                        }
                    }
                });

                break;

            default:
                break;
        }

        return convertView;
    }
}