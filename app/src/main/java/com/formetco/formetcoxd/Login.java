package com.formetco.formetcoxd;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by austinsweat on 7/10/15.
 */
public class Login extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        ModelManager.activitySetup(this, false);

        final EditText emailField = (EditText) findViewById(R.id.loginFieldEmail);
        final EditText passField = (EditText) findViewById(R.id.loginFieldPassword);
        final CheckBox rememberMeField = (CheckBox) findViewById(R.id.loginFieldRememberMe);
        final SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.loginFieldProgressBar);

        Button privacyBtn = (Button) findViewById(R.id.loginFieldPrivacyPolicyBtn);
        Button needAccountBtn = (Button) findViewById(R.id.loginFieldNeedAccountBtn);
        Button submitBtn = (Button) findViewById(R.id.loginFieldSubmitBtn);
        Button forgotBtn = (Button) findViewById(R.id.loginFieldForgotBtn);

        String username = pref.getString("EMAILADDRESS", null);
        String password = pref.getString("PASSWORD", null);

        if (username != null && !username.isEmpty()) {
            emailField.setText(username);
        }

        if (password != null && !password.isEmpty()) {
            passField.setText(password);
        }

        final ActionBarActivity actionBarActivity = this;
        privacyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModalWebView.url = String.format("http://www.formetco.com/app/legal/Privacy.html");
                actionBarActivity.startActivity(
                        new android.content.Intent(
                                actionBarActivity.getApplicationContext(),
                                ModalWebView.class
                        )
                );
            }
        });

        needAccountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModalWebView.url = String.format("http://www.formetco.com/account.php");
                actionBarActivity.startActivity(
                        new android.content.Intent(
                                actionBarActivity.getApplicationContext(),
                                ModalWebView.class
                        )
                );
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (emailField.getText().equals("") || passField.getText().equals("") || emailField.length() == 0 || passField.length() == 0){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(actionBarActivity);
                    alertDialog.setTitle("A-X-ess Denied.");
                    alertDialog.setMessage("Sorry, please check your login info or try again later. If you need support email developer@formetco.com or call (800) 367-6382.");
                    alertDialog.setPositiveButton("Okay",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alertDialog1 = alertDialog.create();
                    alertDialog1.show();
                }else{
                    Toast.makeText(actionBarActivity.getApplicationContext(), "Attempting login...", Toast.LENGTH_SHORT);
                    final String emailaddress = emailField.getText().toString();
                    String password = passField.getText().toString();

                    if(rememberMeField.isChecked()){
                        pref.edit().putString("EMAILADDRESS", emailaddress).putString("PASSWORD", password).commit();
                    }

                    try {
                        progressBar.setVisibility(View.VISIBLE);
                        AsyncHttpClient httpClient = new AsyncHttpClient();
                        httpClient.get("http://f4xdigitalbillboards.com/app/api.php?cmd=login&email=" + emailaddress + "&password=" + password, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                String response = new String(responseBody);
                                Log.d("Login Response", response);
                                try {
                                    JSONObject jObj = new JSONObject(response);
                                    if (!jObj.getString("Token").equals("Invalid")) {
                                        ModelManager.userToken = jObj.get("Token").toString();
                                        ModelManager.userEmail = emailaddress;
                                        actionBarActivity.finish();
                                    } else {
                                /*Notify user of invalid login attempt.*/
                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(actionBarActivity);
                                        alertDialog.setTitle("A-X-ess Denied.");
                                        alertDialog.setMessage("Sorry, please check your login info or try again later. If you need support email developer@formetco.com or call (800) 367-6382.");
                                        alertDialog.setPositiveButton("Okay",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });

                                        AlertDialog alertDialog1 = alertDialog.create();
                                        alertDialog1.show();
                                    }
                                } catch (JSONException e) {
                            /*Server Down, or JSON Problem!*/
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                                    builder1.setMessage("You must have an internet connection to continue.");
                                    builder1.setCancelable(true);
                                    builder1.setPositiveButton("Okay",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });

                                    AlertDialog alert11 = builder1.create();
                                    alert11.show();
                                }
                                progressBar.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                Log.d("Failed", "We failed");
                                AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                                builder1.setMessage("You must have an internet connection to continue.");
                                builder1.setCancelable(true);
                                builder1.setPositiveButton("Okay",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });

                                AlertDialog alert11 = builder1.create();
                                alert11.show();
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                        });
                    }catch (Exception e){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                        builder1.setTitle("Please send screenshot to Developer.");
                        builder1.setMessage(e.getMessage());
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Okay",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

        forgotBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String emailaddress = emailField.getText().toString();
                AsyncHttpClient httpClient = new AsyncHttpClient();
                httpClient.get("http://f4xdigitalbillboards.com/app/api.php?cmd=resetpassword&email=" + emailaddress + "&version=2", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            /*Password reset message.*/
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                        builder1.setTitle("Password reset");
                        builder1.setMessage("A new password has been sent to your email.");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Okay",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            /*Throw error message.*/
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(actionBarActivity.getApplicationContext());
                        builder1.setMessage("You must have an internet connection to continue.");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Okay",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                });
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }
}
