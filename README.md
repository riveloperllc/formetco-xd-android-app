# README #

This mobile application is designed for Android devices and is specifically tailored for our client who specializes in outdoor advertising billboard supplies. This company is located inside the United States and operates independently from other outdoor advertising companies. 

Key features of this app include monitoring, artwork preview, access to online documents, account creation, and drag and drop location designer. 

Riveloper retains exclusive authorship rights to all code and any records provided to third party development teams is considered confidential. 

### What is this repository for? ###

* Quick summary
* Version 1.0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

No contributions are accepted from outside sources without prior written request or authorization.

### Who do I talk to? ###

Riveloper, LLC
855-267-2777
info@riveloper.com